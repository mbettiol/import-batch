package com.enel.workbeat.fnd.batch.rest;

import java.util.List;

import javax.batch.runtime.JobExecution;
import javax.batch.runtime.JobInstance;
import javax.batch.runtime.Metric;
import javax.batch.runtime.StepExecution;

import org.mapstruct.Mapper;

import com.enel.workbeat.fnd.batch.core.CommonBatchCustomDateMapper;

@Mapper(uses={CommonBatchCustomDateMapper.class}, componentModel="cdi")
public interface CommonBatchMapper {
	
	JobExecutionDTO mapper (JobExecution jobExecution);
	List<JobExecutionDTO> mapper (List<JobExecution> jobExecutions);
	JobIstanceDTO mapper (JobInstance jobInstance);
	JobExecutionStepDTO toMapper (StepExecution jobStepExecution);
	List<JobExecutionStepDTO> toMapper (List<StepExecution> jobStepExecution);
	MetricDTO toMapper (Metric metric);	
}
