package com.enel.workbeat.fnd.batch.rest;

import java.io.Serializable;

import javax.batch.runtime.Metric.MetricType;

public class MetricDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private MetricType type;
	private String value;
	
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public MetricType getType() {
		return type;
	}

	public void setType(MetricType type) {
		this.type = type;
	}

 

 

}
