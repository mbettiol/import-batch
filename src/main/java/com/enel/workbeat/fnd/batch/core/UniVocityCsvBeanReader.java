package com.enel.workbeat.fnd.batch.core;

import java.io.IOException;
import java.io.Reader;
import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.univocity.parsers.common.ParsingContext;
import com.univocity.parsers.common.ResultIterator;
import com.univocity.parsers.csv.CsvParserSettings;
import com.univocity.parsers.csv.CsvRoutines;

public class UniVocityCsvBeanReader<T>{ 

	private static Logger log = LoggerFactory.getLogger(UniVocityCsvBeanReader.class);
	
	private final int rowToRead ;

	private int rowNumber=1;

	private final Reader reader;

	private Class<T> beanType;

	private ResultIterator<T, ParsingContext>  iterator;

	public UniVocityCsvBeanReader(Reader reader, int startRow, Class<T> beanType) {
		super();
		this.rowToRead = startRow;
		this.reader = reader;
		this.beanType=beanType;
		initIterator();
	}

	public T read(){
		if(rowNumber==1) {
			//TODO validate header
			String[] parsedHeaders = iterator.getContext().parsedHeaders();
//			iterator.getContext().
		}
		fastRead();
		if (iterator.hasNext()){
			rowNumber++;
			log.debug("Reading BeanType {} RowNumber {}", beanType, rowNumber);
			return iterator.next();
		}
		return null;
	}

	public void close () throws IOException{
		if (reader!=null){
			reader.close();
		}
	}

	public Integer getRowRead(){
		return rowNumber;
	}

	private void fastRead(){
		while (iterator.hasNext() && rowNumber < rowToRead) {
			iterator.next();
			rowNumber++;
			
		}
	}

	private void initIterator() {
		CsvRoutines routines = new CsvRoutines(getCsvPreference()); 
		iterator = routines.iterate(beanType, reader).iterator();
	}

	private CsvParserSettings getCsvPreference(){
		CsvParserSettings parserSettings = new CsvParserSettings();
//		parserSettings.getFormat().setLineSeparator("\n");
//		parserSettings.getFormat().setDelimiter(',');
		parserSettings.setDelimiterDetectionEnabled(true, ',',';'); //TODO play
		parserSettings.setQuoteDetectionEnabled(true);
		parserSettings.setHeaderExtractionEnabled(true); //TODO testme
		return parserSettings;
	}


}

