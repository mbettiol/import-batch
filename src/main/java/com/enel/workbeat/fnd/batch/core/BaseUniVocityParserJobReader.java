package com.enel.workbeat.fnd.batch.core;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;

import javax.batch.api.BatchProperty;
import javax.batch.api.chunk.ItemReader;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class BaseUniVocityParserJobReader<T> implements ItemReader {

	protected Logger log = LoggerFactory.getLogger(BaseUniVocityParserJobReader.class);

	@Inject
	@BatchProperty
	private String filePath;
//	
//    @Inject
//    private JobContext jobContext;
	
	private Class<T> beanCsv;

	private UniVocityCsvBeanReader<T> delegateReader;

	public BaseUniVocityParserJobReader(Class<T> beanCsv) {
		this.beanCsv = beanCsv;
	}

	@Override
	public void open(Serializable prevCheckpointInfo) throws IOException{
		
//		BatchRuntime.getJobOperator().getParameters(jobContext.getInstanceId())
		int startRowNumber = prevCheckpointInfo == null ? 1 : (Integer) prevCheckpointInfo;
		log.trace("Init batch Type {} startRowNumber {} prevCheckPoint {} at {}",beanCsv, startRowNumber, prevCheckpointInfo,LocalDateTime.now());
		InputStreamReader inputStreamReader = getInputStream(filePath);
		delegateReader = new UniVocityCsvBeanReader<T>(inputStreamReader, startRowNumber,beanCsv);
	}

	@Override
	public Object readItem() throws Exception {
 		return delegateReader.read();
	}

	@Override
	public Integer checkpointInfo() { 
		log.trace("Commit batch Type {} at ChunkRowNumber {}",beanCsv, delegateReader.getRowRead());
		return  delegateReader.getRowRead();
	}

	@Override
	public void close() throws IOException  {
		if (delegateReader != null) {
			delegateReader.close();
		}
	}
	
	private InputStreamReader getInputStream(final String inputResource) throws UnsupportedEncodingException, FileNotFoundException {  
		return new InputStreamReader(new FileInputStream(inputResource),"UTF-8");
	}

}
