package com.enel.workbeat.fnd.batch.jberet.core;

import java.util.Iterator;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

@ApplicationScoped
public class JobDescriptorResolver {

	@Inject
	private Instance<JobDescriptor> jobs;
	
 	public JobDescriptor jobDescriptor(String flowName) {
 		Iterator<JobDescriptor> iterator = jobs.iterator();
 		while(iterator.hasNext()) {
 			JobDescriptor next = iterator.next();
 			if(next.getJobName().equals(flowName)) {
 				return next;
 			}
 		}
 		throw new RuntimeException("no such flow: "+flowName);
 		
 	}
}
