package com.enel.workbeat.fnd.batch.rest;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import io.swagger.jaxrs.config.BeanConfig;

@ApplicationPath("api/v1.0")
public class RestConfig extends Application {
	
	 public RestConfig() {
	        BeanConfig beanConfig = new BeanConfig();
	        beanConfig.setVersion("1.0.2");
	        beanConfig.setSchemes(new String[]{"http"});
	        beanConfig.setBasePath("/common-batch/api/v1.0");
	        beanConfig.setResourcePackage(BatchControllerRest.class.getPackage().getName());
	        beanConfig.setTitle("Workbeat Batch Rest Service");
	        beanConfig.setDescription("Workbeat Batch Rest Service");
	        beanConfig.setScan(true);
	    }

    @Override
    public Set<Class<?>> getClasses() {
        HashSet<Class<?>> resources = new HashSet<Class<?>>();
//        resources.add(CorsFilter.class);
        resources.add(io.swagger.jaxrs.listing.ApiListingResource.class);
        resources.add(io.swagger.jaxrs.listing.SwaggerSerializers.class);
        resources.add(io.swagger.jaxrs.listing.AcceptHeaderApiListingResource.class);
        resources.add(io.swagger.jaxrs.listing.BaseApiListingResource.class);
        resources.add(BatchControllerRest.class);
        
//        resources.add(com.enel.workbeat.common.batch.support.WbBatchExceptionMapper.class);
        return resources;
    }
}
