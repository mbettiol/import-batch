package com.enel.workbeat.fnd.batch.rest;

import java.io.Serializable;
import java.util.List;

public class JobIstanceDTO implements Serializable {

 
	private static final long serialVersionUID = 1L;
	private Long instanceId;
	private String jobName;
	private List<JobExecutionDTO> jobExecutions;

	public Long getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(Long instanceId) {
		this.instanceId = instanceId;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public List<JobExecutionDTO> getJobExecutions() {
		return jobExecutions;
	}

	public void setJobExecutions(List<JobExecutionDTO> jobExecutions) {
		this.jobExecutions = jobExecutions;
	}

}
