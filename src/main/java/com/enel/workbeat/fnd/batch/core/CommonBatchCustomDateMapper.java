package com.enel.workbeat.fnd.batch.core;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.mapstruct.Mapper;

@Mapper(componentModel="cdi")
public abstract class CommonBatchCustomDateMapper {
	
	public String toString (Date date){
		if (date !=null){
			Format formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
			return  formatter.format(date);
		}
		return null;
	}

}
