package com.enel.workbeat.fnd.batch.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.batch.operations.JobOperator;
import javax.batch.runtime.BatchRuntime;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.enel.workbeat.fnd.batch.jberet.core.JobDescriptor;

@Path("/flow")
@Produces(MediaType.APPLICATION_JSON)
public class BatchControllerRest extends CommonUtilBatchRest{

 	@GET
	@Path("/{flow-name}/status/{job-id}")
	public Response statusSingleJob(@PathParam("job-id") String jobId, @PathParam("flow-name") String flowName) {
		return executeStatusSingleJob(jobId,flowName);
	}

 	@POST
 	@Path("/{flow-name}/restart/{job-id}/{execution-id}")
	public Response restartSingleJob(@PathParam("job-id") Long instanceId,@PathParam("execution-id") Long executionId, @PathParam("flow-name") String flowName) {
		return execRestartSingleJob(flowName,instanceId, executionId);
	}

 	@POST
 	@Path("/{flow-name}/stop/{job-id}/{execution-id}")
	public Response stopSingleJob(@PathParam("job-id") Long instanceId,@PathParam("execution-id") Long executionId, @PathParam("flow-name") String flowName) {
		return execStopExecutionId(flowName,instanceId, executionId);
	}

	@POST
 	@Path("/{flow-name}/start")
	public Response startJob(@QueryParam("file-name") String fileName,@PathParam("flow-name") String flowName) {
		JobDescriptor jobDescriptor = getJobDescriptor(flowName);
		Properties startProperties = jobDescriptor.createJobParameters(fileName);
		return executeStartJob(flowName,startProperties);
	}
 	
 	@GET
	@Path("/{flow-name}/status")
	public Response statusJob(@PathParam("flow-name") String flowName) {
		return getStatusJob(flowName);
	}
 	
 	@GET
 	@Path("/all")
 	public Response getJobFlows() {
 		JobOperator jobOperator = BatchRuntime.getJobOperator();
 		Set<String> jobNames = jobOperator.getJobNames();
 		List<String> names= new ArrayList<>();
 		names.addAll(jobNames);
 		return Response.ok(jobNames).build();
 	}


}
