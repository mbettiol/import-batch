package com.enel.workbeat.fnd.batch.jberet.core;

import java.io.Serializable;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class AbstractEntityFlowTracker<T extends Serializable> {

	@Id
	private T businessKey;
	
	
	public T getBusinessKey() {
		return businessKey;
	}


	public void setBusinessKey(T businessKey) {
		this.businessKey = businessKey;
	}


	enum State{
		OK,
		ERROR,
		ND
	}
	
}
