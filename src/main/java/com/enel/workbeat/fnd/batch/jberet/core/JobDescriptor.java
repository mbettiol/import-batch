package com.enel.workbeat.fnd.batch.jberet.core;

import java.util.Properties;

import com.enel.workbeat.fnd.batch.core.Constants;

public interface JobDescriptor {

	public String getJobName();
	
	public String getJobFolderPath();
	
	default Properties createJobParameters(String fileName) {
		Properties jobParameters = new Properties();
		jobParameters.setProperty(Constants.JOB_PROPERTY_FILE_PATH, System.getProperty(getJobFolderPath()+"/"+fileName));
		return jobParameters;
	}
}
