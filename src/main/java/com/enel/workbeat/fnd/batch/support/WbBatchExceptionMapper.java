package com.enel.workbeat.fnd.batch.support;

import java.util.HashMap;

import javax.batch.operations.BatchRuntimeException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Provider
public class WbBatchExceptionMapper implements ExceptionMapper<BatchRuntimeException> {

	private static Logger log = LoggerFactory.getLogger(WbBatchExceptionMapper.class);

	@Override
	public Response toResponse(BatchRuntimeException wbExcept) {
		HashMap<Object, Object> hashMap = new HashMap<>();
		hashMap.put("type", "PERMAMENT");
		hashMap.put("code", "TODO");
		hashMap.put("message",wbExcept.getMessage());
		log.error("Exception in Batch ",wbExcept);
		return Response.status(Status.BAD_REQUEST).entity(hashMap).build();
	}
}
