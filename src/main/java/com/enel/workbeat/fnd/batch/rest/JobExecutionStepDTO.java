package com.enel.workbeat.fnd.batch.rest;

import java.io.Serializable;

import javax.batch.runtime.BatchStatus;

public class JobExecutionStepDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private long stepExecutionId;
	private String stepName;
	private BatchStatus batchStatus;
	private String startTime;
	private String endTime;
	private String exitStatus;
	private MetricDTO[] metrics;

	public long getStepExecutionId() {
		return stepExecutionId;
	}

	public void setStepExecutionId(long stepExecutionId) {
		this.stepExecutionId = stepExecutionId;
	}

	public String getStepName() {
		return stepName;
	}

	public void setStepName(String stepName) {
		this.stepName = stepName;
	}

	public BatchStatus getBatchStatus() {
		return batchStatus;
	}

	public void setBatchStatus(BatchStatus batchStatus) {
		this.batchStatus = batchStatus;
	}

	public String getExitStatus() {
		return exitStatus;
	}

	public void setExitStatus(String exitStatus) {
		this.exitStatus = exitStatus;
	}

	public MetricDTO[] getMetrics() {
		return metrics;
	}

	public void setMetrics(MetricDTO[] metrics) {
		this.metrics = metrics;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	
	
}
