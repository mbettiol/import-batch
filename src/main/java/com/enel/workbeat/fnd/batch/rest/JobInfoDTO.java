package com.enel.workbeat.fnd.batch.rest;

import java.io.Serializable;
import java.util.List;

public class JobInfoDTO implements Serializable {

 
	private static final long serialVersionUID = 1L;

	private List<JobIstanceDTO> jobIstances;

	public List<JobIstanceDTO> getJobIstances() {
		return jobIstances;
	}

	public void setJobIstances(List<JobIstanceDTO> jobIstances) {
		this.jobIstances = jobIstances;
	}

}
