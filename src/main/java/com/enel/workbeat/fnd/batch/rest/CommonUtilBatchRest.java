package com.enel.workbeat.fnd.batch.rest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javax.batch.operations.JobOperator;
import javax.batch.runtime.BatchRuntime;
import javax.batch.runtime.JobExecution;
import javax.batch.runtime.JobInstance;
import javax.batch.runtime.StepExecution;
import javax.inject.Inject;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.enel.workbeat.fnd.batch.jberet.core.JobDescriptor;
import com.enel.workbeat.fnd.batch.jberet.core.JobDescriptorResolver;

public abstract class CommonUtilBatchRest {

	private static Logger log = LoggerFactory.getLogger(CommonUtilBatchRest.class);

	@Inject
	private JobDescriptorResolver jobDescriptorResolver;
	
	@Inject 
	private CommonBatchMapper commonBatchMapper;

	protected Response executeStartJob(String flowName, Properties jobParameters) {
		JobOperator jobOperator = BatchRuntime.getJobOperator();
		log.info("Properties {}",jobParameters);
		long jobId = jobOperator.start(flowName, jobParameters);
		return Response.accepted(convertDTO(Arrays.asList(jobOperator.getJobInstance(jobId)))).build();
	}

	protected Response executeStatusSingleJob(String flowName, String jobId) {
		JobOperator jobOperator = BatchRuntime.getJobOperator();
		List<JobInstance> jobInstances = jobOperator.getJobInstances(flowName, 0, 1000);
		JobInstance jobInstance=null;
		for (JobInstance jobI:jobInstances){
			if ( jobI!=null && jobI.getInstanceId()==Long.parseLong(jobId)){
				jobInstance=jobI;
				break;
			}			
		}
		return Response.ok((jobInstance!=null)?convertDTO(Arrays.asList(jobInstance)):new JobInfoDTO()).build();
	}

	protected Response execRestartSingleJob(String flowName, Long instanceId,Long executionId) {
		JobOperator jobOperator = BatchRuntime.getJobOperator();
		JobInstance jobInstance = getJobInstanceByInstanceId(jobOperator,instanceId,flowName);
		if (executionId!=null && jobInstance!=null) 
		{
			List<JobExecution> jobExecutions = jobOperator.getJobExecutions(jobInstance);
			JobExecution jobExecution = jobExecutions.stream().filter(jobE -> executionId.compareTo(jobE.getExecutionId())==0).findFirst().orElse(null);
			if (jobExecution!=null){
				log.info("Restart instanceID :{} executionId: {}",jobInstance.getInstanceId(),jobExecution.getExecutionId());
				jobOperator.restart(jobExecution.getExecutionId(), jobOperator.getParameters(jobExecution.getExecutionId()));
			}else{
				log.warn("The Restart executionId {} not found for the instanceID :{}",executionId,jobInstance.getInstanceId());
				return Response.status(Status.NOT_FOUND).entity("{ \"msg\":\"The Restart executionId "+executionId+" not found for the instanceID :"+jobInstance.getInstanceId()+"\"}").build();
			}
		} 
		return Response.ok(convertDTO(Arrays.asList(jobInstance))).build();
	}

	protected Response execStopExecutionId(String flowName,Long instanceId,Long executionId) {
		JobOperator jobOperator = BatchRuntime.getJobOperator();
		JobInstance jobInstance = getJobInstanceByInstanceId(jobOperator,instanceId, flowName);
		if (executionId!=null && jobInstance!=null) 
		{
			List<JobExecution> jobExecutions = jobOperator.getJobExecutions(jobInstance);
			JobExecution jobExecution = jobExecutions.stream().filter(jobE -> executionId.compareTo(jobE.getExecutionId())==0).findFirst().orElse(null);
			if (jobExecution!=null){
				log.info("Stopping instanceID :{} executionId: {}",jobInstance.getInstanceId(),jobExecution.getExecutionId());
				jobOperator.stop(jobExecution.getExecutionId());
			}else{
				log.warn("The Restart executionId {} not found for the instanceID :{}",executionId,jobInstance.getInstanceId());
				return Response.status(Status.NOT_FOUND).entity("{ \"msg\":\"The Stop executionId "+executionId+" not found for the instanceID :"+jobInstance.getInstanceId()+"\"}").build();
			}
		} 
		return Response.ok(convertDTO(Arrays.asList(jobInstance))).build();
	}
	
	protected Response getStatusJob(String flowName) {
		JobOperator jobOperator = BatchRuntime.getJobOperator();
		List<JobInstance> instances = jobOperator.getJobInstances(flowName, 
				0, jobOperator.getJobInstanceCount(flowName));
		JobInfoDTO jobInfoDTO = convertDTO(instances);
		return Response.ok(jobInfoDTO).build();
	}


	private JobInstance getJobInstanceByInstanceId(JobOperator jobOperator,Long jobId, String flowName){
		List<JobInstance> jobInstances = jobOperator.getJobInstances(flowName, 0, 1000);
		JobInstance jobInstance=null;
		for (JobInstance jobI:jobInstances){
			if ( jobI!=null && jobI.getInstanceId()==jobId){
				jobInstance=jobI;
				break;
			}			
		}
		return jobInstance;
	}

	protected JobIstanceDTO convertDTO (JobInstance jobInstance){
		JobOperator jobOperator = BatchRuntime.getJobOperator();
		List<JobExecution> jobExecutions = jobOperator.getJobExecutions(jobInstance);
		
		JobIstanceDTO jobIstanceDTO = commonBatchMapper.mapper(jobInstance);		
		List<JobExecutionDTO> jobExecutionDTOs = commonBatchMapper.mapper(jobExecutions);
		for (JobExecutionDTO jobExecution : jobExecutionDTOs) {
			Long id=jobExecution.getExecutionId();
			jobExecution.setJobParameters(jobOperator.getParameters(id));
		}
		jobIstanceDTO.setJobExecutions(jobExecutionDTOs);
		jobExecutionDTOs.stream().forEach(jobExecut -> {
			 List<StepExecution> stepsExecution = jobOperator.getStepExecutions(jobExecut.getExecutionId());
			 jobExecut.setJobExecutionSteps(commonBatchMapper.toMapper(stepsExecution));
		});
		
		return jobIstanceDTO;
	}

	protected JobInfoDTO convertDTO (List<JobInstance> jobInstances){
		JobInfoDTO jobInfoDTO = new JobInfoDTO();
		List<JobIstanceDTO> jobIstanceDTOs = new ArrayList<>();
		for (JobInstance jobInstance : jobInstances) {
			JobIstanceDTO jobIstanceDTO  = convertDTO(jobInstance);
			jobIstanceDTOs.add(jobIstanceDTO);
		}
		jobInfoDTO.setJobIstances(jobIstanceDTOs);
		return jobInfoDTO;
	}
	
	protected JobDescriptor getJobDescriptor(String flowName) {
		return jobDescriptorResolver.jobDescriptor(flowName);
	}
}
