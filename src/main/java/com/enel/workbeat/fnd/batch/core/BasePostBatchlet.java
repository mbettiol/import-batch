package com.enel.workbeat.fnd.batch.core;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;

import javax.batch.api.Batchlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class BasePostBatchlet implements Batchlet {

	protected Logger log = LoggerFactory.getLogger(Batchlet.class);
 	
 	public String moveFileToProcessed(String filePath) throws IOException{
 		Path pathAnagraficFile = Paths.get(filePath);
 		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HHmmss");    
 		Date resultdate = new Date(System.currentTimeMillis());
 		Path destFile = Paths.get(filePath + "_" + sdf.format(resultdate)+ ".processed");
		Files.move(pathAnagraficFile, destFile);
		log.trace("End batch at {}",LocalDateTime.now());
		return "end";
	}
 	
	@Override
	public void stop() throws Exception {
		// empty
	}
 

}
