package com.enel.workbeat.fnd.batch.jberet.core;

import javax.persistence.EntityManager;

public interface EntityManagerResolver {

	public EntityManager getEntityManager();
}
