package com.enel.workbeat.fnd.batch.rest;

import java.io.Serializable;
import java.util.List;
import java.util.Properties;

import javax.batch.runtime.BatchStatus;

public class JobExecutionDTO implements Serializable{

	private static final long serialVersionUID = 1L;

	private Long executionId;

	private String jobName;

	private BatchStatus batchStatus;

	private String startTime;

	private String endTime;

	private String exitStatus;

	private String createTime;
	
	private String lastUpdatedTime;
	
	private Properties  jobParameters;

	private List<JobExecutionStepDTO> jobExecutionSteps;
	
	public Properties getJobParameters() {
		return jobParameters;
	}

	public void setJobParameters(Properties jobParameters) {
		this.jobParameters = jobParameters;
	}

	public Long getExecutionId() {
		return executionId;
	}

	public void setExecutionId(Long executionId) {
		this.executionId = executionId;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public BatchStatus getBatchStatus() {
		return batchStatus;
	}

	public void setBatchStatus(BatchStatus batchStatus) {
		this.batchStatus = batchStatus;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getExitStatus() {
		return exitStatus;
	}

	public void setExitStatus(String exitStatus) {
		this.exitStatus = exitStatus;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getLastUpdatedTime() {
		return lastUpdatedTime;
	}

	public void setLastUpdatedTime(String lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}

	public List<JobExecutionStepDTO> getJobExecutionSteps() {
		return jobExecutionSteps;
	}

	public void setJobExecutionSteps(List<JobExecutionStepDTO> list) {
		this.jobExecutionSteps = list;
	}

//	public List<StepExecution> getStepExecutions() {
//		return stepExecutions;
//	}
//
//	public void setStepExecutions(List<StepExecution> stepExecutions) {
//		this.stepExecutions = stepExecutions;
//	}




}
