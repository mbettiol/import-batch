package com.enel.workbeat.fnd.batch.jberet.core;

public interface Validator {

	public Validation validate(Object object);
}
