package com.enel.workbeat.fnd.batch.jberet.core;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "sys_batch_tracker", schema = "wb")
public class BatchFlowTracker {

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "seq_batch_tracker")
    @SequenceGenerator(name = "seq_batch_tracker", sequenceName = "wb.seq_batch_tracker")
    @Column(name = "id_log_detail")
	private Long idBatchTracker;
	
	private String flowName;
	private ExcecutionStatus executionStatus;
	private Outcome outcome;
	private boolean acknowledgedError;
	private boolean acknowledgedNotes;
	private String errors;
	private String filename;
	private String absolutePath;
	private Date startTime;
	private Date endTime;
	private Long jobInstanceId;
	private Long jobExecutionId;
	private Long countToProcess;
	private Long countSuccess;
	private Long countFail;
	private Long countSkip;
	private String csvHeader;
	
	public enum ExcecutionStatus{
		EXECUTING,
		TERMINATED
	}
	public enum Outcome{
		PREVALIDATION_FAIL,
		COMPLETED
	}
	public Long getIdBatchTracker() {
		return idBatchTracker;
	}
	public void setIdBatchTracker(Long idBatchTracker) {
		this.idBatchTracker = idBatchTracker;
	}
	public String getFlowName() {
		return flowName;
	}
	public void setFlowName(String flowName) {
		this.flowName = flowName;
	}
	public ExcecutionStatus getExecutionStatus() {
		return executionStatus;
	}
	public void setExecutionStatus(ExcecutionStatus executionStatus) {
		this.executionStatus = executionStatus;
	}
	public Outcome getOutcome() {
		return outcome;
	}
	public void setOutcome(Outcome outcome) {
		this.outcome = outcome;
	}
	public boolean isAcknowledgedError() {
		return acknowledgedError;
	}
	public void setAcknowledgedError(boolean acknowledgedError) {
		this.acknowledgedError = acknowledgedError;
	}
	public boolean isAcknowledgedNotes() {
		return acknowledgedNotes;
	}
	public void setAcknowledgedNotes(boolean acknowledgedNotes) {
		this.acknowledgedNotes = acknowledgedNotes;
	}
	public String getErrors() {
		return errors;
	}
	public void setErrors(String errors) {
		this.errors = errors;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getAbsolutePath() {
		return absolutePath;
	}
	public void setAbsolutePath(String absolutePath) {
		this.absolutePath = absolutePath;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public Long getJobInstanceId() {
		return jobInstanceId;
	}
	public void setJobInstanceId(Long jobInstanceId) {
		this.jobInstanceId = jobInstanceId;
	}
	public Long getJobExecutionId() {
		return jobExecutionId;
	}
	public void setJobExecutionId(Long jobExecutionId) {
		this.jobExecutionId = jobExecutionId;
	}
	public Long getCountToProcess() {
		return countToProcess;
	}
	public void setCountToProcess(Long countToProcess) {
		this.countToProcess = countToProcess;
	}
	public Long getCountSuccess() {
		return countSuccess;
	}
	public void setCountSuccess(Long countSuccess) {
		this.countSuccess = countSuccess;
	}
	public Long getCountFail() {
		return countFail;
	}
	public void setCountFail(Long countFail) {
		this.countFail = countFail;
	}
	public Long getCountSkip() {
		return countSkip;
	}
	public void setCountSkip(Long countSkip) {
		this.countSkip = countSkip;
	}
	public String getCsvHeader() {
		return csvHeader;
	}
	public void setCsvHeader(String csvHeader) {
		this.csvHeader = csvHeader;
	}
	
	
	
	
}
