package com.enel.workbeat.fnd.csvparser.test;

import java.io.StringReader;

import org.junit.Assert;
import org.junit.Test;

import com.enel.workbeat.fnd.batch.core.UniVocityCsvBeanReader;
import com.univocity.parsers.annotations.BooleanString;
import com.univocity.parsers.annotations.Headers;
import com.univocity.parsers.annotations.Parsed;
import com.univocity.parsers.common.DataProcessingException;
import com.univocity.parsers.common.ParsingContext;
import com.univocity.parsers.common.ResultIterator;
import com.univocity.parsers.common.TextParsingException;
import com.univocity.parsers.common.processor.BeanProcessor;
import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;
import com.univocity.parsers.csv.CsvRoutines;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

public class UnivocityTest {

	@Test
	public void test() {
		CsvParserSettings parserSettings = new CsvParserSettings();
		parserSettings.getFormat().setLineSeparator("\n");
		parserSettings.getFormat().setDelimiter(',');
		parserSettings.setHeaderExtractionEnabled(true);
		parserSettings.setDelimiterDetectionEnabled(true);
		
		BeanProcessor<TestBean> strict = createStrict(TestBean.class);
		
		

		parserSettings.setProcessor(strict);
//		parserSettings.se
		
		CsvParser csvParser = new CsvParser(parserSettings);
		
		
			CsvRoutines routines = new CsvRoutines(parserSettings); 
//			routines.par
			StringReader reader = new StringReader("DESCRIZIONE,NUMERO,DESCRIZIONE\nciao,mitico");
		
//			routines.
				
		try {
		csvParser.beginParsing(reader);
		while(csvParser.parseNextRecord()!=null) {
//			strict.setStrictHeaderValidationEnabled(strictHeaderValidationEnabled);
		}
		}finally {
			csvParser.stopParsing();
		}
//		Bea
//		ResultIterator<TestBean, ParsingContext> iterator = routines.iterate(TestBean.class, reader).iterator();
//		while(iterator.hasNext()) {
//			TestBean next = iterator.next();
//			ParsingContext context = iterator.getContext();
//			context.headers();
//			System.out.println(next.toString());
//		}
		
	}
	
//	@Headers(extract=true)
	public static class TestBean{
		
		@Parsed(field="DESCRIZIONE")
		private String descrizione;
		
		@Parsed(field="NUMERO")
		private String numero;

		public String getDescrizione() {
			return descrizione;
		}

		public void setDescrizione(String descrizione) {
			this.descrizione = descrizione;
		}

		public String getNumero() {
			return numero;
		}

		public void setNumero(String numero) {
			this.numero = numero;
		}

		@Override
		public String toString() {
			return "TestBean [descrizione=" + descrizione + ", numero=" + numero + "]";
		}
		
		
	}
	
	public static class SimpleBeanProcessor<T> extends BeanProcessor<T>{

		public SimpleBeanProcessor(Class<T> beanType) {
			super(beanType);
		}

		@Override
		public void beanProcessed(T bean, ParsingContext context) {
		}
		
	}
	
	public static <T> BeanProcessor<T> createStrict(Class<T> clazz){
		SimpleBeanProcessor<T> simpleBeanProcessor = new SimpleBeanProcessor<>(clazz);
		simpleBeanProcessor.setStrictHeaderValidationEnabled(true);
		return simpleBeanProcessor;
	}
	
	
	
	@Test
	public void parseErrors() {
		//TODO mi aspettavo fallisse...
		try {
		StringReader stringReader = new StringReader("\"codice\",\"notBoolean\n12345,ciao");
		UniVocityCsvBeanReader delegateReader = new UniVocityCsvBeanReader<MyBean>(stringReader, 1,MyBean.class);
		Object read = delegateReader.read();
		}catch (TextParsingException e) {
			e.toString();
			throw e;
		}
	}
	
	@Test
	public void validationErrors() {
		
		try {
		StringReader stringReader = new StringReader("codice,notBoolean\n12345,ciao");
		UniVocityCsvBeanReader delegateReader = new UniVocityCsvBeanReader<MyBean>(stringReader, 1,MyBean.class);
		Object read = delegateReader.read();
		read.toString();
		}catch (DataProcessingException e) {
			String columnName = e.getColumnName();
			long lineIndex = e.getLineIndex();
			String message = e.getMessage();
			// TODO: handle exception
			throw e;
		}
	}
	
	@Test
	public void validateFormat1() {
		StringReader stringReader = new StringReader("codice,notBoolean\n12345,true");
		UniVocityCsvBeanReader delegateReader = new UniVocityCsvBeanReader<MyBean>(stringReader, 1,MyBean.class);
		MyBean read = (MyBean) delegateReader.read();
		Assert.assertEquals("12345", read.codice);
		Assert.assertEquals(true, read.valore);
	}
	
	@Test
	public void validateFormat2() {
		StringReader stringReader = new StringReader("\"codice\",\"notBoolean\"\n\"12345\",\"true\"");
		UniVocityCsvBeanReader delegateReader = new UniVocityCsvBeanReader<MyBean>(stringReader, 1,MyBean.class);
		MyBean read = (MyBean) delegateReader.read();
		Assert.assertEquals("12345", read.codice);
		Assert.assertEquals(true, read.valore);
	}
	
	@Test
	public void validateFormat3() {
		StringReader stringReader = new StringReader("\"codice\";\"notBoolean\"\n\"12345\";\"true\"");
		UniVocityCsvBeanReader delegateReader = new UniVocityCsvBeanReader<MyBean>(stringReader, 1,MyBean.class);
		MyBean read = (MyBean) delegateReader.read();
		Assert.assertEquals("12345", read.codice);
		Assert.assertEquals(true, read.valore);
	}
	
	//a capo in mezzo con escape
	@Test
	public void validateFormat4() {
		StringReader stringReader = new StringReader("\"codice\";\"notBoolean\";\"new_column\"\n\"12\n345\";\"true\"");
		UniVocityCsvBeanReader delegateReader = new UniVocityCsvBeanReader<MyBean>(stringReader, 1,MyBean.class);
		MyBean read = (MyBean) delegateReader.read();
		Assert.assertEquals("12\n345", read.codice);
		Assert.assertEquals(true, read.valore);
	}
	
	
	
	//a capo in mezzo senza escape
	@Test
	public void validateFormat5() {
		StringReader stringReader = new StringReader("\"codice\";\"notBoolean\"\n12\n345;\"true\"");
		UniVocityCsvBeanReader delegateReader = new UniVocityCsvBeanReader<MyBean>(stringReader, 1,MyBean.class);
		MyBean read = (MyBean) delegateReader.read();
		Assert.assertEquals("12\n345", read.codice);
		Assert.assertEquals(true, read.valore);
	}
	
	//a capo in mezzo senza escape
	@Test
	public void validateFormat6() {
		StringReader stringReader = new StringReader("\"codice\";\"notBoolean\"\n\"12\n;345\";\"true\"");
		UniVocityCsvBeanReader delegateReader = new UniVocityCsvBeanReader<MyBean>(stringReader, 1,MyBean.class);
		MyBean read = (MyBean) delegateReader.read();
		Assert.assertEquals("12\n;345", read.codice);
		Assert.assertEquals(true, read.valore);
	}
	
	@Test
	public void testConRigheVuote() {
		StringReader stringReader = new StringReader("\"codice\";\"notBoolean\"\n\"12345\";\"true\"\n\r\n\n\n\"6789\";false");
		UniVocityCsvBeanReader delegateReader = new UniVocityCsvBeanReader<MyBean>(stringReader, 1,MyBean.class);
		MyBean read = (MyBean) delegateReader.read();
		Assert.assertEquals("12345", read.codice);
		Assert.assertEquals(true, read.valore);
		read = (MyBean) delegateReader.read();
		Assert.assertEquals("6789", read.codice);
		Assert.assertEquals(false, read.valore);
		read = (MyBean) delegateReader.read();
		Assert.assertNull(read);
	}
	
	
	public static class MyBean{
		
		@Parsed(field = "codice")
		String codice;
		
		@Parsed(field="notBoolean")
		@BooleanString(falseStrings="false", trueStrings="true")
		boolean valore;
	}
}
