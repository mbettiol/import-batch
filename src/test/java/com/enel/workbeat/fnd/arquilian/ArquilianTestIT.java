package com.enel.workbeat.fnd.arquilian;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

import javax.batch.runtime.BatchRuntime;
import javax.enterprise.context.ApplicationScoped;
import javax.persistence.Persistence;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.archive.importer.MavenImporter;
import org.junit.Before;
import org.junit.BeforeClass;
//import org.jboss.weld.environment.se.Weld;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;

import com.enel.workbeat.fnd.baseimpl.AbstractIT;
import com.enel.workbeat.fnd.baseimpl.ArquilianUtils;
import com.enel.workbeat.fnd.batch.core.Constants;
//import com.enel.workbeat.fnd.junit4.MyWeldInitiator;
import com.enel.workbeat.fnd.flusso1.JobDescriptorFlusso1;

@RunWith(Arquillian.class)
public class ArquilianTestIT extends AbstractIT{
	  
    @Rule
    public TemporaryFolder folder = new TemporaryFolder();
    
    @Deployment()
    public static WebArchive createDeployment() throws IOException {
        WebArchive webArchive = ShrinkWrap.create(MavenImporter.class).loadPomFromFile("pom.xml").importBuildOutput()
            .as(WebArchive.class)
            .addPackage(AbstractIT.class.getPackage())
            .addPackage(JobDescriptorFlusso1.class.getPackage())
            .addAsWebInfResource(new File("src/test/resources/test-ds.xml"), "test-ds.xml");
        
        final Path resources_dir = Paths.get("src", "test", "resources");
        Files.walk(resources_dir)
          .forEach((p) ->{ webArchive.addAsResource(p.toFile(),
        		  resources_dir.relativize(p).toString());
          		  System.out.println(resources_dir.relativize(p).toString());});
           
//            .addAsResource(new File("src/main/resources/test-persistence.xml"),"META-INF/persistence.xml").
//            .addAsResource(new File("src/main/resources/beans.xml"),"META-INF/beans.xml");
        
        
        return webArchive;
        
    }
    
    @Before
    public void cleanDatabase() {
    	ArquilianUtils.clearDatabase();
    }

    
    
//	  @Rule
//	  public MyWeldInitiator weld = MyWeldInitiator.from(new Weld("jberet-weld")) 
//	  .setPersistenceContextFactory(ip -> Persistence.createEntityManagerFactory("jberet-pu").createEntityManager())
//	  .activate(ApplicationScoped.class).build();
//	  
	  @Test
	  public void emptyFileTest() throws Exception {
		  Path path = ArquilianUtils.copyToExternalFile(folder, "measurement-point-flow", "emptyfile.csv");
		  Properties properties = new Properties();
		  properties.put(Constants.JOB_PROPERTY_FILE_PATH, path.toFile().toString());
		  this.params = properties;
		  startJobAndWait("measurement-point-flow");
		  
		  this.jobExecution.getStepExecutions();
		  BatchRuntime.getJobOperator().getJobInstance(this.jobExecutionId);
		  assertNoException();
		  assertCompleted();
		  
	  }
//	  
	  @Test
	  public void someRowsTest() throws Exception {
//		  Class.forName("org.h2.Driver");
		  Path path = ArquilianUtils.copyToExternalFile(folder, "measurement-point-flow", "somerows.csv");
		  
		  Properties properties = new Properties();
		  properties.put(Constants.JOB_PROPERTY_FILE_PATH, path.toFile().toString());
		  this.params = properties;
		  startJobAndWait("measurement-point-flow");
		  
		  this.jobExecution.getStepExecutions();
		  BatchRuntime.getJobOperator().getJobInstance(this.jobExecutionId);
		  assertNoException();
		  assertCompleted();
		  
	  }
	  
}
