package com.enel.workbeat.fnd.arquilian;

import javax.inject.Inject;

import org.apache.deltaspike.cdise.api.CdiContainer;
import org.apache.deltaspike.cdise.api.CdiContainerLoader;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.testenricher.cdi.CDIInjectionEnricher;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.enel.workbeat.fnd.batch.jberet.core.EntityManagerResolver;

public class ContainerlessTest {

	@Inject
	private EntityManagerResolver er;
	
	@Test
	public void test() {
		CdiContainer cdiContainer = CdiContainerLoader.getCdiContainer();	
		cdiContainer.boot();
	}
}
