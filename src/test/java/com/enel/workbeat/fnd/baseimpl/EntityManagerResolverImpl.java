package com.enel.workbeat.fnd.baseimpl;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.enel.workbeat.fnd.batch.jberet.core.EntityManagerResolver;

@ApplicationScoped
public class EntityManagerResolverImpl implements EntityManagerResolver{

	@PersistenceContext
	private EntityManager em;
	
	@Produces
	@Override
	public EntityManager getEntityManager() {
		return em;
	}

}
