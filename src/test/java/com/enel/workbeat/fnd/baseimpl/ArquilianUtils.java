package com.enel.workbeat.fnd.baseimpl;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.junit.rules.TemporaryFolder;

public class ArquilianUtils {

	public static Path copyToExternalFile(TemporaryFolder folder, String dir, String filename) throws IOException {
		 FileInputStream fis = (FileInputStream) Thread.currentThread().getContextClassLoader().getResourceAsStream(dir+"/"+filename);
		 Path target = folder.newFolder(dir).toPath().resolve(filename);
		 System.out.println("creating test file : "+target);
		 Files.copy(fis, target);
		 return target;
	}
	
	public static void clearDatabase() {
		try {
			InitialContext initialContext = new InitialContext();
			DataSource datasource = (DataSource)initialContext.lookup("java:/test-ds");
			try(Connection c = datasource.getConnection()){
				try(Statement s = c.createStatement()){
	
					// Disable FK
					s.execute("SET REFERENTIAL_INTEGRITY FALSE");
	
					// Find all tables and truncate them
					Set<String> tables = new HashSet<String>();
					try(ResultSet rs = s.executeQuery("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES  where TABLE_SCHEMA='PUBLIC'")){
						while (rs.next()) {
							tables.add(rs.getString(1));
						}
					}
					for (String table : tables) {
						s.executeUpdate("TRUNCATE TABLE " + table);
					}
	
					// Idem for sequences
					Set<String> sequences = new HashSet<String>();
					try(ResultSet rs = s.executeQuery("SELECT SEQUENCE_NAME FROM INFORMATION_SCHEMA.SEQUENCES WHERE SEQUENCE_SCHEMA='PUBLIC'")){
						while (rs.next()) {
							sequences.add(rs.getString(1));
						}
					}
					for (String seq : sequences) {
						s.executeUpdate("ALTER SEQUENCE " + seq + " RESTART WITH 1");
					}
	
					// Enable FK
					s.execute("SET REFERENTIAL_INTEGRITY TRUE");
				}
			}
		}catch(SQLException | NamingException e){
			ExceptionUtils.throwException(e);
		}

	}
}
