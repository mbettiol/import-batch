package com.enel.workbeat.fnd.flusso1;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.enel.workbeat.fnd.batch.jberet.core.AbstractEntityFlowTracker;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name="measurement_point_flow_tracker", schema="wb")
public class MeasurementPointFlowTracker extends AbstractEntityFlowTracker<String> {

	@Column(name="point_code")
	private String pointCode;
}
