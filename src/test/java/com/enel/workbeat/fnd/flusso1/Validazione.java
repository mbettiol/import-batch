package com.enel.workbeat.fnd.flusso1;

import javax.batch.api.Batchlet;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Named("Validazione")
public class Validazione implements Batchlet{

	private Logger LOGGER = LoggerFactory.getLogger(Validazione.class);
	
	
	public Validazione() {
		
	}
	
	@Override
	public String process() throws Exception {
		LOGGER.info("processing");
		return null;
	}

	@Override
	public void stop() throws Exception {
		
	}

}
