package com.enel.workbeat.fnd.flusso1;

import java.time.LocalDateTime;

import com.univocity.parsers.conversions.Conversion;

public class CustomConversionLocalDateTime implements Conversion<String, LocalDateTime>{

	@Override
	public LocalDateTime execute(String input) {
		if  (input !=null){
			return LocalDateTime.parse(input);
		}
		return null;
	}

	@Override
	public String revert(LocalDateTime input) {
		return null;
	}

}
