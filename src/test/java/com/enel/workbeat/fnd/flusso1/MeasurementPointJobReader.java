package com.enel.workbeat.fnd.flusso1;

import javax.inject.Named;

import com.enel.workbeat.fnd.batch.core.BaseUniVocityParserJobReader;

@Named(value = "measurementPointJobReader")
public class MeasurementPointJobReader extends BaseUniVocityParserJobReader<MeasurementPointDE> {

	public MeasurementPointJobReader() {
		super(MeasurementPointDE.class);
	}
}
