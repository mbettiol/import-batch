package com.enel.workbeat.fnd.flusso1;

import java.util.List;

import javax.batch.api.chunk.AbstractItemWriter;
import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.enel.workbeat.fnd.batch.jberet.core.EntityManagerResolver;

@Named(value = "measurementPointJobWriter")
public class MeasurementPointJobWriter extends AbstractItemWriter {

	private static Logger log = LoggerFactory.getLogger(MeasurementPointJobWriter.class);

	@Inject
	private EntityManagerResolver emr;

	@Override
	public void writeItems(List<Object> items) throws Exception {
		log.debug("Call patronSaint service to persist Items {}", items.size());
	
		for(Object o : items) {
			MeasurementPointDE de = (MeasurementPointDE) o;
			
			MeasurementPointFlowTracker measurementPointFlowTracker = new MeasurementPointFlowTracker();
			measurementPointFlowTracker.setBusinessKey(de.getPointCode());
			emr.getEntityManager().persist(measurementPointFlowTracker);
		}
		emr.getEntityManager().flush();
	}

}
