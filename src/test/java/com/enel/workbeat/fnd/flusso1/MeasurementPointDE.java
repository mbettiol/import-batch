package com.enel.workbeat.fnd.flusso1;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.univocity.parsers.annotations.Convert;
import com.univocity.parsers.annotations.Parsed;

/**
 * DTO MdMeasurement Point da usare esclusivamente per flusso FULL (CSV file) e DELTA per allineamento XSD
 * Link : http://confluence.enelint.global/pages/viewpage.action?pageId=24454836
 * Versione : 29
 * 
 */
public class MeasurementPointDE implements Serializable {

	private static final long serialVersionUID = 1L;

	@Parsed(field = "point_code")
	private String pointCode;

	/**** START - Custom override per gestire CSV e XML e mapping ****/
	// Per rinominare il campo al fine di gestire il mapping con mupstruct
	@Parsed(field = "id_md_measurement_point")
	@JsonProperty(value="idMdMeasurementPoint")
	private BigInteger idMdMeasurementPointHeartBeat;

	// override per DTO json flusso DELTA
	@JsonProperty("mnotes")
	@Parsed(field = "m_notes")
	private String mNotes;

	@JsonProperty(value = "snotes")
	@Parsed(field = "s_notes")
	private String sNotes;

	@Parsed(field = "date_2g_activation")
	@JsonProperty(value="date2GActivation")
	@Convert(conversionClass = CustomConversionLocalDateTime.class)
	private LocalDateTime date2gActivation;

	@JsonProperty(value="flgMeterGeneration2G")
	@Parsed(field = "flg_meter_generation_2g")
	private Boolean flgMeterGeneration2g;
	/**** END- Custom override ****/

	@Parsed(field = "eid_customer")
	private String eidCustomer;

	@Parsed(field = "name")
	private String name;

	@Parsed(field = "surname")
	private String surname;

	@Parsed(field = "company_name")
	private String companyName;

	@Parsed(field = "vat_number")
	private String vatNumber;

	@Parsed(field = "tax_code")
	private String taxCode;

	@Parsed(field = "phone_number")
	private String phoneNumber;

	@Parsed(field = "fax_number")
	private String faxNumber;

	@Parsed(field = "email_address")
	private String emailAddress;

	@Parsed(field = "certified_email")
	private String certifiedEmail;

	@Parsed(field = "cod_customer_category")
	private String codCustomerCategory;

	@Parsed(field = "cod_customer_entity_type")
	private String codCustomerEntityType;

	@Parsed(field = "dt_birth")
	@Convert(conversionClass = CustomConversionLocalDateTime.class)
	private LocalDateTime dtBirth;

	@Parsed(field = "birth_place")
	private String birthPlace;

	@Parsed(field = "ca_street")
	private String caStreet;

	@Parsed(field = "ca_address_number")
	private String caAddressNumber;

	@Parsed(field = "ca_stair")
	private String caStair;

	@Parsed(field = "ca_floor")
	private String caFloor;

	@Parsed(field = "ca_flat_number")
	private String caFlatNumber;

	@Parsed(field = "ca_locality")
	private String caLocality;

	@Parsed(field = "ca_prefix")
	private String caPrefix;

	@Parsed(field = "ca_municipality")
	private String caMunicipality;

	@Parsed(field = "ca_district")
	private String caDistrict;

	@Parsed(field = "ca_region")
	private String caRegion;

	@Parsed(field = "ca_nation")
	private String caNation;

	@Parsed(field = "ca_zip_code")
	private String caZipCode;

	@Parsed(field = "ca_cod_istat")
	private String caCodIstat;

	@Parsed(field = "ca_care_of")
	private String caCareOf;

	@Parsed(field = "eid_dispatching")
	private String eidDispatching;

	@Parsed(field = "distribution_company")
	private String distributionCompany;

	@Parsed(field = "transmission_component_calculation")
	private String transmissionComponentCalculation;

	@Parsed(field = "exemption_item")
	private String exemptionItem;

	@Parsed(field = "eid_exemption_certificate")
	private String eidExemptionCertificate;

	@Parsed(field = "eid_exemption_certificate_enterprise")
	private String eidExemptionCertificateEnterprise;

	@Parsed(field = "dt_exemption_certificate")
	@Convert(conversionClass = CustomConversionLocalDateTime.class)
	private LocalDateTime dtExemptionCertificate;

	@Parsed(field = "dt_start_exemption")
	@Convert(conversionClass = CustomConversionLocalDateTime.class)
	private LocalDateTime dtStartExemption;

	@Parsed(field = "dt_end_exemption")
	@Convert(conversionClass = CustomConversionLocalDateTime.class)
	private LocalDateTime dtEndExemption;

	@Parsed(field = "dt_enterprise_registration")
	@Convert(conversionClass = CustomConversionLocalDateTime.class)
	private LocalDateTime dtEnterpriseRegistration;

	@Parsed(field = "number_of_delivery_point")
	private String numberOfDeliveryPoint;

	@Parsed(field = "flg_ndp_installation")
	private Boolean flgNdpInstallation;

	@Parsed(field = "flg_ndp_readings_collection")
	private Boolean flgNdpReadingsCollection;

	@Parsed(field = "flg_ndp_readings_verification")
	private Boolean flgNdpReadingsVerification;

	@Parsed(field = "flg_unplugging")
	private Boolean flgUnplugging;

	@Parsed(field = "cod_unplugging_type")
	private String codUnpluggingType;

	@Parsed(field = "unplugging_document_reference")
	private String unpluggingDocumentReference;

	@Parsed(field = "unplugging_request_author")
	private String unpluggingRequestAuthor;

	@Parsed(field = "flg_lha_self_certification")
	private Boolean flgLhaSelfCertification;

	@Parsed(field = "emergency_plan_phone_number")
	private String emergencyPlanPhoneNumber;

	@Parsed(field = "flg_emergency_plan_self_certification")
	private Boolean flgEmergencyPlanSelfCertification;

	@Parsed(field = "flg_customs_autorization")
	private Boolean flgCustomsAutorization;

	@Parsed(field = "piecework_annual_power")
	private BigDecimal pieceworkAnnualPower;

	@Parsed(field = "eid_exercise_regulation")
	private String eidExerciseRegulation;

	@Parsed(field = "dt_start_exercise_regulation")
	@Convert(conversionClass = CustomConversionLocalDateTime.class)
	private LocalDateTime dtStartExerciseRegulation;

	@Parsed(field = "power_exercise_regulation")
	private BigDecimal powerExerciseRegulation;

	@Parsed(field = "responsible_meter_maintenance")
	private String responsibleMeterMaintenance;

	@Parsed(field = "responsible_measure_mngmt")
	private String responsibleMeasureMngmt;

	@Parsed(field = "cod_sspc_point_type")
	private String codSspcPointType;

	@Parsed(field = "associated_point_code")
	private String associatedPointCode;

	@Parsed(field = "cod_contract_type")
	private String codContractType;

	@Parsed(field = "point_of_delivery")
	private String pointOfDelivery;

	@Parsed(field = "eneltel")
	private String eneltel;

	@Parsed(field = "cod_operational_unit")
	private String codOperationalUnit;

	@Parsed(field = "cod_territorial_division")
	private String codTerritorialDivision;

	@Parsed(field = "measure_voltage")
	private BigInteger measureVoltage;

	@Parsed(field = "cod_electric_phases")
	private String codElectricPhases;

	@Parsed(field = "cod_voltage_type")
	private String codVoltageType;

	@Parsed(field = "power_contractual")
	private BigDecimal powerContractual;

	@Parsed(field = "power_available")
	private BigDecimal powerAvailable;

	@Parsed(field = "power_allowance")
	private BigDecimal powerAllowance;

	@Parsed(field = "power_production")
	private BigDecimal powerProduction;

	@Parsed(field = "power_production_allowance")
	private BigDecimal powerProductionAllowance;

	@Parsed(field = "flg_power_measurement")
	private Boolean flgPowerMeasurement;

	@Parsed(field = "flg_elevator")
	private Boolean flgElevator;

	@Parsed(field = "flg_breaker")
	private Boolean flgBreaker;

	@Parsed(field = "cod_supply_type")
	private String codSupplyType;

	@Parsed(field = "flg_flat_rate_billing")
	private Boolean flgFlatRateBilling;

	@Parsed(field = "flg_seasonal_supply")
	private Boolean flgSeasonalSupply;

	@Parsed(field = "dt_seasonal_supply_start")

	@Convert(conversionClass = CustomConversionLocalDateTime.class)
	private LocalDateTime dtSeasonalSupplyStart;

	@Parsed(field = "dt_seasonal_supply_end")
	@Convert(conversionClass = CustomConversionLocalDateTime.class)
	private LocalDateTime dtSeasonalSupplyEnd;

	@Parsed(field = "number_of_time_slot")
	private BigInteger numberOfTimeSlot;

	@Parsed(field = "eid_tariff")
	private String eidTariff;

	@Parsed(field = "cod_connection_type")
	private String codConnectionType;

	@Parsed(field = "cod_energy_use")
	private String codEnergyUse;

	@Parsed(field = "cod_payload_curves_type")
	private String codPayloadCurvesType;

	@Parsed(field = "cod_commodities_sector")
	private String codCommoditiesSector;

	@Parsed(field = "reduction_factor")
	private BigInteger reductionFactor;

	@Parsed(field = "cod_supply_state")
	private String codSupplyState;

	@Parsed(field = "cod_payment_status")
	private String codPaymentStatus;

	@Parsed(field = "flg_compliance_status")
	private Boolean flgComplianceStatus;

	@Parsed(field = "flg_checking_status")
	private Boolean flgCheckingStatus;

	@Parsed(field = "cod_reading_frequency")
	private String codReadingFrequency;

	@Parsed(field = "cod_connection_contract_type")
	private String codConnectionContractType;

	@Parsed(field = "flg_only_input")
	private Boolean flgOnlyInput;

	@Parsed(field = "cod_measurement_point_type")
	private String codMeasurementPointType;

	@Parsed(field = "eid_censimp_group")
	private String eidCensimpGroup;

	@Parsed(field = "eid_censimp_meter")
	private String eidCensimpMeter;

	@Parsed(field = "cod_installation_service_type")
	private String codInstallationServiceType;

	@Parsed(field = "eid_measurement_point")
	private String eidMeasurementPoint;

	@Parsed(field = "eid_censimp_production_plant")
	private String eidCensimpProductionPlant;

	@Parsed(field = "eid_production_section")
	private String eidProductionSection;

	@Parsed(field = "eid_production_unit")
	private String eidProductionUnit;

	@Parsed(field = "flg_temporary_exercise")
	private Boolean flgTemporaryExercise;

	@Parsed(field = "cod_detachment_type")
	private String codDetachmentType;

	@Parsed(field = "cod_detachment_result")
	private String codDetachmentResult;

	@Parsed(field = "measure_mngmnt_technical")
	private String measureMngmntTechnical;

	@Parsed(field = "measure_mngmnt_commercial")
	private String measureMngmntCommercial;

	@Parsed(field = "flg_measurement_service")
	private Boolean flgMeasurementService;

	@Parsed(field = "dt_supply_start")
	@Convert(conversionClass = CustomConversionLocalDateTime.class)
	private LocalDateTime dtSupplyStart;

	@Parsed(field = "cod_connection_point_type")
	private String codConnectionPointType;

	@Parsed(field = "cod_power_measurement_type")
	private String codPowerMeasurementType;

	@Parsed(field = "sa_street")
	private String saStreet;

	@Parsed(field = "sa_address_number")
	private String saAddressNumber;

	@Parsed(field = "sa_stair")
	private String saStair;

	@Parsed(field = "sa_floor")
	private String saFloor;

	@Parsed(field = "sa_flat_number")
	private String saFlatNumber;

	@Parsed(field = "sa_locality")
	private String saLocality;

	@Parsed(field = "sa_prefix")
	private String saPrefix;

	@Parsed(field = "sa_municipality")
	private String saMunicipality;

	@Parsed(field = "sa_district")
	private String saDistrict;

	@Parsed(field = "sa_nation")
	private String saNation;

	@Parsed(field = "sa_zip_code")
	private String saZipCode;

	@Parsed(field = "sa_cod_istat")
	private String saCodIstat;

	@Parsed(field = "sa_care_of")
	private String saCareOf;

	@Parsed(field = "eid_socket")
	private String eidSocket;

	@Parsed(field = "cod_socket_position_type")
	private String codSocketPositionType;

	@Parsed(field = "cp_cod_electric_phases")
	private String cpCodElectricPhases;

	@Parsed(field = "cable_section")
	private BigDecimal cableSection;

	@Parsed(field = "access_instruction")
	private String accessInstruction;

	@Parsed(field = "substation_distance")
	private BigInteger substationDistance;

	@Parsed(field = "eid_substation")
	private String eidSubstation;

	@Parsed(field = "substation_name")
	private String substationName;

	@Parsed(field = "cod_substation_type")
	private String codSubstationType;

	@Parsed(field = "cod_optic_fiber_status_type")
	private String codOpticFiberStatusType;

	@Parsed(field = "gps_coordinate_x")
	private String gpsCoordinateX;

	@Parsed(field = "gps_coordinate_y")
	private String gpsCoordinateY;

	@Parsed(field = "eid_network_point")
	private String eidNetworkPoint;

	@Parsed(field = "measurement_point_description")
	private String measurementPointDescription;



	@Parsed(field = "meter_serial_number")
	private String meterSerialNumber;

	@Parsed(field = "meter_code")
	private String meterCode;

	@Parsed(field = "gage_limiter")
	private String gageLimiter;

	@Parsed(field = "cosine_t")
	private BigDecimal cosineT;

	@Parsed(field = "electronic_unit_register_number")
	private String electronicUnitRegisterNumber;

	//FIXME
//	@Convert(conversionClass=CustomParsePMPFlgElectronicUnit.class)
//	@Parsed(field = "flg_electronic_unit")
//	private Boolean flgElectronicUnit;

	@Parsed(field = "register_disi_disconnectivity")
	private BigInteger registerDisiDisconnectivity;

	@Parsed(field = "digits_active_energy")
	private BigInteger digitsActiveEnergy;

	@Parsed(field = "digits_power")
	private BigInteger digitsPower;

	@Parsed(field = "digits_reactive_energy")
	private BigInteger digitsReactiveEnergy;

	@Parsed(field = "software_version")
	private String softwareVersion;

	private String hardwareVersion;

	@Parsed(field = "register_tsen_antitamper")
	private BigInteger registerTsenAntitamper;

	@Parsed(field = "register_tsenmag_antitamper")
	private BigInteger registerTsenmagAntitamper;

	@Parsed(field = "cod_protocol_type")
	private String codProtocolType;

	@Parsed(field = "flg_technical_bidirectional")
	private Boolean flgTechnicalBidirectional;

	@Parsed(field = "anomalies_irreg_list")
	private String anomaliesIrregList;

	@Parsed(field = "cod_finds_envelope")
	private String codFindsEnvelope;

	@Parsed(field = "cod_meter_type")
	private String codMeterType;

	@Parsed(field = "dt_installation")
	@Convert(conversionClass = CustomConversionLocalDateTime.class)
	private LocalDateTime dtInstallation;

	@Parsed(field = "voltage")
	private BigDecimal voltage;

	//FIXME
//	@Parsed(field = "flg_positive_measure")
//	@Convert(conversionClass=CustomParsePMFlgPositiveMeasure.class)
//	private Boolean flgPositiveMeasure;

	@Parsed(field = "builder_serial_number")
	private String builderSerialNumber;

	@Parsed(field = "com_module_protocol")
	private String comModuleProtocol;

	@Parsed(field = "com_module_frame")
	private String comModuleFrame;

	@Parsed(field = "cs_phone_number")
	private String csPhoneNumber;

	@Parsed(field = "password")
	private String password;

//	@Convert(conversionClass=CustomParsePMProfile.class)
//	@Parsed(field = "profile")
//	private BigInteger profile;

	@Parsed(field = "cod_meter_comm_manager_type")
	private String codMeterCommManagerType;

	@Parsed(field = "plate_serial_number")
	private String plateSerialNumber;

	@Parsed(field = "flg_igme")
	private Boolean flgIgme;

	@Parsed(field = "registers_measure_units")
	private String registersMeasureUnits;

	@Parsed(field = "internal_constant_cdc")
	private BigDecimal internalConstantCdc;

	@Parsed(field = "internal_constant_registers")
	private BigDecimal internalConstantRegisters;

	@Parsed(field = "flg_use_builder_serial_number")
	private Boolean flgUseBuilderSerialNumber;

	@Parsed(field = "multidrop_string")
	private String multidropString;

	@Parsed(field = "cod_meter_owner")
	private String codMeterOwner;

	@Parsed(field = "meter_class")
	private BigDecimal meterClass;

	@Parsed(field = "cod_module_builder")
	private String codModuleBuilder;

	@Parsed(field = "cod_module_model")
	private String codModuleModel;

	@Parsed(field = "cod_module_communication_type")
	private String codModuleCommunicationType;

	@Parsed(field = "technical_contact_name")
	private String technicalContactName;

	@Parsed(field = "technical_contact_phone")
	private String technicalContactPhone;

	@Parsed(field = "flg_password_tele_reading")
	private Boolean flgPasswordTeleReading;

	@Parsed(field = "eid_builder")
	private String eidBuilder;

	@Parsed(field = "eid_model")
	private String eidModel;

	@Parsed(field = "eid_firmware_version")
	private String eidFirmwareVersion;

	@Parsed(field = "flg_committing")
	private Boolean flgCommitting;

	@Parsed(field = "construction_year")
	private BigInteger constructionYear;

	@Parsed(field = "flg_antitamper_activated")
	private Boolean flgAntitamperActivated;

	@Parsed(field = "flg_mag_antitamper_activated")
	private Boolean flgMagAntitamperActivated;

	@Parsed(field = "power_sum_constant")
	private BigDecimal powerSumConstant;

	@Parsed(field = "energy_constant")
	private BigDecimal energyConstant;

	@Parsed(field = "power_constant")
	private BigDecimal powerConstant;

	@Parsed(field = "full_scale")
	private BigDecimal fullScale;

	@Parsed(field = "ca_serial_number_phase_1")
	private String caSerialNumberPhase1;

	@Parsed(field = "ca_serial_number_phase_2")
	private String caSerialNumberPhase2;

	@Parsed(field = "ca_serial_number_phase_3")
	private String caSerialNumberPhase3;

	@Parsed(field = "ca_serial_number_phase_4")
	private String caSerialNumberPhase4;

	@Parsed(field = "ka_ampere_ratio")
	private BigDecimal kaAmpereRatio;

	@Parsed(field = "ca_primary_ratio")
	private BigDecimal caPrimaryRatio;

	@Parsed(field = "ca_secondary_ratio")
	private BigDecimal caSecondaryRatio;

	@Parsed(field = "va_serial_number_phase_1")
	private String vaSerialNumberPhase1;

	@Parsed(field = "va_serial_number_phase_2")
	private String vaSerialNumberPhase2;

	@Parsed(field = "va_serial_number_phase_3")
	private String vaSerialNumberPhase3;

	@Parsed(field = "kv_voltage_ratio")
	private BigDecimal kvVoltageRatio;

	@Parsed(field = "va_primary_ratio")
	private BigInteger vaPrimaryRatio;

	@Parsed(field = "va_secondary_ratio")
	private BigInteger vaSecondaryRatio;

	@Parsed(field = "kapi")
	private BigInteger kapi;

	@Parsed(field = "kasi")
	private BigInteger kasi;

	@Parsed(field = "constant_ka_x_kv")
	private BigDecimal constantKaXKv;

	@Parsed(field = "ca_brand_phase_1")
	private String caBrandPhase1;

	@Parsed(field = "ca_brand_phase_2")
	private String caBrandPhase2;

	@Parsed(field = "ca_brand_phase_3")
	private String caBrandPhase3;

	@Parsed(field = "ca_brand_phase_4")
	private String caBrandPhase4;

	@Parsed(field = "ca_model_phase_1")
	private String caModelPhase1;

	@Parsed(field = "ca_model_phase_2")
	private String caModelPhase2;

	@Parsed(field = "ca_model_phase_3")
	private String caModelPhase3;

	@Parsed(field = "ca_model_phase_4")
	private String caModelPhase4;

	@Parsed(field = "ca_class_phase_1")
	private String caClassPhase1;

	@Parsed(field = "ca_class_phase_2")
	private String caClassPhase2;

	@Parsed(field = "ca_class_phase_3")
	private String caClassPhase3;

	@Parsed(field = "ca_class_phase_4")
	private String caClassPhase4;

	@Parsed(field = "va_brand_phase_1")
	private String vaBrandPhase1;

	@Parsed(field = "va_brand_phase_2")
	private String vaBrandPhase2;

	@Parsed(field = "va_brand_phase_3")
	private String vaBrandPhase3;

	@Parsed(field = "va_model_phase_1")
	private String vaModelPhase1;

	@Parsed(field = "va_model_phase_2")
	private String vaModelPhase2;

	@Parsed(field = "va_model_phase_3")
	private String vaModelPhase3;

	@Parsed(field = "va_class_phase_1")
	private String vaClassPhase1;

	@Parsed(field = "va_class_phase_2")
	private String vaClassPhase2;

	@Parsed(field = "va_class_phase_3")
	private String vaClassPhase3;

	@Parsed(field = "overchargeability_percentage")
	private BigInteger overchargeabilityPercentage;

	@Parsed(field = "cod_device_owner_type")
	private String codDeviceOwnerType;

	@Parsed(field = "cod_module_protocol_type")
	private String codModuleProtocolType;

	@Parsed(field = "flg_external_module")
	private Boolean flgExternalModule;

	@Parsed(field = "flg_pstn_protection")
	private Boolean flgPstnProtection;

	@Parsed(field = "ccid")
	private String ccid;

	@Parsed(field = "pin")
	private String pin;

	@Parsed(field = "puk")
	private String puk;

	@Parsed(field = "assignee_name")
	private String assigneeName;

	@Parsed(field = "dt_assign")

	@Convert(conversionClass = CustomConversionLocalDateTime.class)
	private LocalDateTime dtAssign;

	@Parsed(field = "imsi")
	private String imsi;

	@Parsed(field = "ipv4")
	private String ipv4;

	@Parsed(field = "ipv6")
	private String ipv6;

	@Parsed(field = "loading_author")
	private String loadingAuthor;

	@Parsed(field = "dt_loading")

	@Convert(conversionClass = CustomConversionLocalDateTime.class)
	private LocalDateTime dtLoading;

	@Parsed(field = "cod_telco_provider")
	private String codTelcoProvider;

	@Parsed(field = "eid_transformer")
	private String eidTransformer;

	@Parsed(field = "autor_creation")
	private String autorCreation;

	@Parsed(field = "eneltel_of_delivery")
	private String eneltelOfDelivery;

	@Parsed(field = "cod_contract_service_type")
	private String codContractServiceType;

	@Parsed(field = "cod_meter_code_type")
	private String codMeterCodeType;

	@Parsed(field = "cod_socket_group")
	private String codSocketGroup;

	@Parsed(field = "dt_meter_removed")
	@Convert(conversionClass = CustomConversionLocalDateTime.class)
	private LocalDateTime dtMeterRemoved;

	@Parsed(field = "eid_trader")
	private BigInteger eidTrader;

	@Parsed(field = "flg_installed")
	private Boolean flgInstalled;

	@Parsed(field = "flg_real_point")
	private Boolean flgRealPoint;

	@Parsed(field = "flg_supended_for_checking")
	private Boolean flgSupendedForChecking;

	@Parsed(field = "cod_energy_balance")
	private String codEnergyBalance;

	@Parsed(field = "k2_key")
	private String k2Key;

	@Parsed(field = "ma_address_number")
	private String maAddressNumber;

	@Parsed(field = "ma_care_of")
	private String maCareOf;

	@Parsed(field = "ma_cod_istat")
	private String maCodIstat;

	@Parsed(field = "ma_district")
	private String maDistrict;

	@Parsed(field = "ma_flat_number")
	private String maFlatNumber;

	@Parsed(field = "ma_floor")
	private String maFloor;

	@Parsed(field = "ma_locality")
	private String maLocality;

	@Parsed(field = "ma_municipality")
	private String maMunicipality;

	@Parsed(field = "ma_nation")
	private String maNation;

	@Parsed(field = "ma_prefix")
	private String maPrefix;

	@Parsed(field = "ma_region")
	private String maRegion;

	@Parsed(field = "ma_stair")
	private String maStair;

	@Parsed(field = "ma_street")
	private String maStreet;

	@Parsed(field = "ma_zip_code")
	private String maZipCode;

	@Parsed(field = "prod_contr_activation_date")
	@Convert(conversionClass = CustomConversionLocalDateTime.class)
	private LocalDateTime prodContrActivationDate;

	@Parsed(field = "prod_contr_end_exercise_regulation_date")
	@Convert(conversionClass = CustomConversionLocalDateTime.class)
	private LocalDateTime prodContrEndExerciseRegulationDate;

	@Parsed(field = "prod_contr_termination_date")
	@Convert(conversionClass = CustomConversionLocalDateTime.class)
	private LocalDateTime prodContrTerminationDate;

	@Parsed(field = "prod_contr_termination_reason")
	private String prodContrTerminationReason;

	@Parsed(field = "prod_contr_transient_power")
	private BigDecimal prodContrTransientPower;

	@Parsed(field = "prod_contr_type")
	private String prodContrType;

	@Parsed(field = "supply_voltage")
	private BigInteger supplyVoltage;

	@Parsed(field = "title_expiration_date")
	@Convert(conversionClass = CustomConversionLocalDateTime.class)
	private LocalDateTime titleExpirationDate;

	@Parsed(field = "trader_cod_market_type")
	private String traderCodMarketType;

	@Parsed(field = "trader_company_name")
	private String traderCompanyName;

	@Parsed(field = "trader_mobile_phone_number")
	private String traderMobilePhoneNumber;

	@Parsed(field = "trader_phone_number")
	private String traderPhoneNumber;

	@Parsed(field = "trader_vat_number")
	private String traderVatNumber;

	@Parsed(field = "va_brand_phase_4")
	private String vaBrandPhase4;

	@Parsed(field = "va_class_phase_4")
	private String vaClassPhase4;

	@Parsed(field = "va_model_phase_4")
	private String vaModelPhase4;

	@Parsed(field = "va_serial_number_phase_4")
	private String vaSerialNumberPhase4;

	@Parsed(field = "cod_module_frame_type")
	private String codModuleFrameType;

	@Parsed(field = "cod_spacer_state")
	private String codSpacerState;

	@Parsed(field = "cod_meter_direction_type")
	private String codMeterDirectionType;

	@Parsed(field = "unit_cdc")
	private String unitCdc;

	@Parsed(field = "cod_direction_type")
	private String codDirectionType;

	@Parsed(field = "flg_reacheable")
	private Boolean flgReacheable;

	@Parsed(field = "meter_serial_number_reactive")
	private String meterSerialNumberReactive;

	@Parsed(field = "meter_code_reactive")
	private String meterCodeReactive;

	@Parsed(field = "meter_serial_number_power")
	private String meterSerialNumberPower;

	@Parsed(field = "meter_code_power")
	private String meterCodePower;

	@Parsed(field = "cod_socket_state")
	private String codSocketState;

	@Parsed(field = "cod_remote_control_type")
	private String codRemoteControlType;

	@Parsed(field = "region")
	private String region;

	@Parsed(field = "timezone")
	private String timezone;

	@Parsed(field = "extractiondate")
	@Convert(conversionClass = CustomConversionLocalDateTime.class)
	private LocalDateTime extractiondate;

	@Parsed(field = "start_reference_date")
	@Convert(conversionClass = CustomConversionLocalDateTime.class)
	private LocalDateTime startReferenceDate;

	@Parsed(field = "end_reference_date")
	@Convert(conversionClass = CustomConversionLocalDateTime.class)
	private LocalDateTime endReferenceDate;
	
	@Parsed(field = "flg_fraud_predisposition")
	private Boolean flgFraudPredisposition;
	
	@Parsed(field = "flg_point_code_del_87_16")
	private Boolean flgPointCodeDel8716;
	
	@Parsed(field = "cod_button_list_type")
	private String codButtonListType;
	
	@Parsed(field = "dt_button_list_reprogramming")
	@Convert(conversionClass = CustomConversionLocalDateTime.class)
	private LocalDateTime dtButtonListReprogramming;
	
	@Parsed(field = "cod_meter_generation")
	private String codMeterGeneration;

	@Convert(conversionClass = CustomConversionLocalDateTime.class)
	@Parsed(field = "dt_synchronization")
	private LocalDateTime dtSynchronization;


	public String getPointCode() {
		return pointCode;
	}

	public void setPointCode(String pointCode) {
		this.pointCode = pointCode;
	}

	public String getEidCustomer() {
		return eidCustomer;
	}

	public void setEidCustomer(String eidCustomer) {
		this.eidCustomer = eidCustomer;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getVatNumber() {
		return vatNumber;
	}

	public void setVatNumber(String vatNumber) {
		this.vatNumber = vatNumber;
	}

	public String getTaxCode() {
		return taxCode;
	}

	public void setTaxCode(String taxCode) {
		this.taxCode = taxCode;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getFaxNumber() {
		return faxNumber;
	}

	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getCertifiedEmail() {
		return certifiedEmail;
	}

	public void setCertifiedEmail(String certifiedEmail) {
		this.certifiedEmail = certifiedEmail;
	}

	public String getCodCustomerCategory() {
		return codCustomerCategory;
	}

	public void setCodCustomerCategory(String codCustomerCategory) {
		this.codCustomerCategory = codCustomerCategory;
	}

	public String getCodCustomerEntityType() {
		return codCustomerEntityType;
	}

	public void setCodCustomerEntityType(String codCustomerEntityType) {
		this.codCustomerEntityType = codCustomerEntityType;
	}

	public LocalDateTime getDtBirth() {
		return dtBirth;
	}

	public void setDtBirth(LocalDateTime dtBirth) {
		this.dtBirth = dtBirth;
	}

	public String getBirthPlace() {
		return birthPlace;
	}

	public void setBirthPlace(String birthPlace) {
		this.birthPlace = birthPlace;
	}

	public String getCaStreet() {
		return caStreet;
	}

	public void setCaStreet(String caStreet) {
		this.caStreet = caStreet;
	}

	public String getCaAddressNumber() {
		return caAddressNumber;
	}

	public void setCaAddressNumber(String caAddressNumber) {
		this.caAddressNumber = caAddressNumber;
	}

	public String getCaStair() {
		return caStair;
	}

	public void setCaStair(String caStair) {
		this.caStair = caStair;
	}

	public String getCaFloor() {
		return caFloor;
	}

	public void setCaFloor(String caFloor) {
		this.caFloor = caFloor;
	}

	public String getCaFlatNumber() {
		return caFlatNumber;
	}

	public void setCaFlatNumber(String caFlatNumber) {
		this.caFlatNumber = caFlatNumber;
	}

	public String getCaLocality() {
		return caLocality;
	}

	public void setCaLocality(String caLocality) {
		this.caLocality = caLocality;
	}

	public String getCaPrefix() {
		return caPrefix;
	}

	public void setCaPrefix(String caPrefix) {
		this.caPrefix = caPrefix;
	}

	public String getCaMunicipality() {
		return caMunicipality;
	}

	public void setCaMunicipality(String caMunicipality) {
		this.caMunicipality = caMunicipality;
	}

	public String getCaDistrict() {
		return caDistrict;
	}

	public void setCaDistrict(String caDistrict) {
		this.caDistrict = caDistrict;
	}

	public String getCaRegion() {
		return caRegion;
	}

	public void setCaRegion(String caRegion) {
		this.caRegion = caRegion;
	}

	public String getCaNation() {
		return caNation;
	}

	public void setCaNation(String caNation) {
		this.caNation = caNation;
	}

	public String getCaZipCode() {
		return caZipCode;
	}

	public void setCaZipCode(String caZipCode) {
		this.caZipCode = caZipCode;
	}

	public String getCaCodIstat() {
		return caCodIstat;
	}

	public void setCaCodIstat(String caCodIstat) {
		this.caCodIstat = caCodIstat;
	}

	public String getCaCareOf() {
		return caCareOf;
	}

	public void setCaCareOf(String caCareOf) {
		this.caCareOf = caCareOf;
	}

	public String getEidDispatching() {
		return eidDispatching;
	}

	public void setEidDispatching(String eidDispatching) {
		this.eidDispatching = eidDispatching;
	}

	public String getDistributionCompany() {
		return distributionCompany;
	}

	public void setDistributionCompany(String distributionCompany) {
		this.distributionCompany = distributionCompany;
	}

	public String getTransmissionComponentCalculation() {
		return transmissionComponentCalculation;
	}

	public void setTransmissionComponentCalculation(String transmissionComponentCalculation) {
		this.transmissionComponentCalculation = transmissionComponentCalculation;
	}

	public String getExemptionItem() {
		return exemptionItem;
	}

	public void setExemptionItem(String exemptionItem) {
		this.exemptionItem = exemptionItem;
	}

	public String getEidExemptionCertificate() {
		return eidExemptionCertificate;
	}

	public void setEidExemptionCertificate(String eidExemptionCertificate) {
		this.eidExemptionCertificate = eidExemptionCertificate;
	}

	public String getEidExemptionCertificateEnterprise() {
		return eidExemptionCertificateEnterprise;
	}

	public void setEidExemptionCertificateEnterprise(String eidExemptionCertificateEnterprise) {
		this.eidExemptionCertificateEnterprise = eidExemptionCertificateEnterprise;
	}

	public LocalDateTime getDtExemptionCertificate() {
		return dtExemptionCertificate;
	}

	public void setDtExemptionCertificate(LocalDateTime dtExemptionCertificate) {
		this.dtExemptionCertificate = dtExemptionCertificate;
	}

	public LocalDateTime getDtStartExemption() {
		return dtStartExemption;
	}

	public void setDtStartExemption(LocalDateTime dtStartExemption) {
		this.dtStartExemption = dtStartExemption;
	}

	public LocalDateTime getDtEndExemption() {
		return dtEndExemption;
	}

	public void setDtEndExemption(LocalDateTime dtEndExemption) {
		this.dtEndExemption = dtEndExemption;
	}

	public LocalDateTime getDtEnterpriseRegistration() {
		return dtEnterpriseRegistration;
	}

	public void setDtEnterpriseRegistration(LocalDateTime dtEnterpriseRegistration) {
		this.dtEnterpriseRegistration = dtEnterpriseRegistration;
	}

	public String getNumberOfDeliveryPoint() {
		return numberOfDeliveryPoint;
	}

	public void setNumberOfDeliveryPoint(String numberOfDeliveryPoint) {
		this.numberOfDeliveryPoint = numberOfDeliveryPoint;
	}

	public Boolean getFlgNdpInstallation() {
		return flgNdpInstallation;
	}

	public void setFlgNdpInstallation(Boolean flgNdpInstallation) {
		this.flgNdpInstallation = flgNdpInstallation;
	}

	public Boolean getFlgNdpReadingsCollection() {
		return flgNdpReadingsCollection;
	}

	public void setFlgNdpReadingsCollection(Boolean flgNdpReadingsCollection) {
		this.flgNdpReadingsCollection = flgNdpReadingsCollection;
	}

	public Boolean getFlgNdpReadingsVerification() {
		return flgNdpReadingsVerification;
	}

	public void setFlgNdpReadingsVerification(Boolean flgNdpReadingsVerification) {
		this.flgNdpReadingsVerification = flgNdpReadingsVerification;
	}

	public Boolean getFlgUnplugging() {
		return flgUnplugging;
	}

	public void setFlgUnplugging(Boolean flgUnplugging) {
		this.flgUnplugging = flgUnplugging;
	}

	public String getCodUnpluggingType() {
		return codUnpluggingType;
	}

	public void setCodUnpluggingType(String codUnpluggingType) {
		this.codUnpluggingType = codUnpluggingType;
	}

	public String getUnpluggingDocumentReference() {
		return unpluggingDocumentReference;
	}

	public void setUnpluggingDocumentReference(String unpluggingDocumentReference) {
		this.unpluggingDocumentReference = unpluggingDocumentReference;
	}

	public String getUnpluggingRequestAuthor() {
		return unpluggingRequestAuthor;
	}

	public void setUnpluggingRequestAuthor(String unpluggingRequestAuthor) {
		this.unpluggingRequestAuthor = unpluggingRequestAuthor;
	}

	public Boolean getFlgLhaSelfCertification() {
		return flgLhaSelfCertification;
	}

	public void setFlgLhaSelfCertification(Boolean flgLhaSelfCertification) {
		this.flgLhaSelfCertification = flgLhaSelfCertification;
	}

	public String getEmergencyPlanPhoneNumber() {
		return emergencyPlanPhoneNumber;
	}

	public void setEmergencyPlanPhoneNumber(String emergencyPlanPhoneNumber) {
		this.emergencyPlanPhoneNumber = emergencyPlanPhoneNumber;
	}

	public Boolean getFlgEmergencyPlanSelfCertification() {
		return flgEmergencyPlanSelfCertification;
	}

	public void setFlgEmergencyPlanSelfCertification(Boolean flgEmergencyPlanSelfCertification) {
		this.flgEmergencyPlanSelfCertification = flgEmergencyPlanSelfCertification;
	}

	public Boolean getFlgCustomsAutorization() {
		return flgCustomsAutorization;
	}

	public void setFlgCustomsAutorization(Boolean flgCustomsAutorization) {
		this.flgCustomsAutorization = flgCustomsAutorization;
	}

	public BigDecimal getPieceworkAnnualPower() {
		return pieceworkAnnualPower;
	}

	public void setPieceworkAnnualPower(BigDecimal pieceworkAnnualPower) {
		this.pieceworkAnnualPower = pieceworkAnnualPower;
	}

	public String getEidExerciseRegulation() {
		return eidExerciseRegulation;
	}

	public void setEidExerciseRegulation(String eidExerciseRegulation) {
		this.eidExerciseRegulation = eidExerciseRegulation;
	}

	public LocalDateTime getDtStartExerciseRegulation() {
		return dtStartExerciseRegulation;
	}

	public void setDtStartExerciseRegulation(LocalDateTime dtStartExerciseRegulation) {
		this.dtStartExerciseRegulation = dtStartExerciseRegulation;
	}

	public BigDecimal getPowerExerciseRegulation() {
		return powerExerciseRegulation;
	}

	public void setPowerExerciseRegulation(BigDecimal powerExerciseRegulation) {
		this.powerExerciseRegulation = powerExerciseRegulation;
	}

	public String getResponsibleMeterMaintenance() {
		return responsibleMeterMaintenance;
	}

	public void setResponsibleMeterMaintenance(String responsibleMeterMaintenance) {
		this.responsibleMeterMaintenance = responsibleMeterMaintenance;
	}

	public String getResponsibleMeasureMngmt() {
		return responsibleMeasureMngmt;
	}

	public void setResponsibleMeasureMngmt(String responsibleMeasureMngmt) {
		this.responsibleMeasureMngmt = responsibleMeasureMngmt;
	}

	public String getCodSspcPointType() {
		return codSspcPointType;
	}

	public void setCodSspcPointType(String codSspcPointType) {
		this.codSspcPointType = codSspcPointType;
	}

	public String getAssociatedPointCode() {
		return associatedPointCode;
	}

	public void setAssociatedPointCode(String associatedPointCode) {
		this.associatedPointCode = associatedPointCode;
	}

	public String getCodContractType() {
		return codContractType;
	}

	public void setCodContractType(String codContractType) {
		this.codContractType = codContractType;
	}

	public String getPointOfDelivery() {
		return pointOfDelivery;
	}

	public void setPointOfDelivery(String pointOfDelivery) {
		this.pointOfDelivery = pointOfDelivery;
	}

	public String getEneltel() {
		return eneltel;
	}

	public void setEneltel(String eneltel) {
		this.eneltel = eneltel;
	}

	public String getCodOperationalUnit() {
		return codOperationalUnit;
	}

	public void setCodOperationalUnit(String codOperationalUnit) {
		this.codOperationalUnit = codOperationalUnit;
	}

	public String getCodTerritorialDivision() {
		return codTerritorialDivision;
	}

	public void setCodTerritorialDivision(String codTerritorialDivision) {
		this.codTerritorialDivision = codTerritorialDivision;
	}

	public BigInteger getMeasureVoltage() {
		return measureVoltage;
	}

	public void setMeasureVoltage(BigInteger measureVoltage) {
		this.measureVoltage = measureVoltage;
	}

	public String getCodElectricPhases() {
		return codElectricPhases;
	}

	public void setCodElectricPhases(String codElectricPhases) {
		this.codElectricPhases = codElectricPhases;
	}

	public String getCodVoltageType() {
		return codVoltageType;
	}

	public void setCodVoltageType(String codVoltageType) {
		this.codVoltageType = codVoltageType;
	}

	public BigDecimal getPowerContractual() {
		return powerContractual;
	}

	public void setPowerContractual(BigDecimal powerContractual) {
		this.powerContractual = powerContractual;
	}

	public BigDecimal getPowerAvailable() {
		return powerAvailable;
	}

	public void setPowerAvailable(BigDecimal powerAvailable) {
		this.powerAvailable = powerAvailable;
	}

	public BigDecimal getPowerAllowance() {
		return powerAllowance;
	}

	public void setPowerAllowance(BigDecimal powerAllowance) {
		this.powerAllowance = powerAllowance;
	}

	public BigDecimal getPowerProduction() {
		return powerProduction;
	}

	public void setPowerProduction(BigDecimal powerProduction) {
		this.powerProduction = powerProduction;
	}

	public BigDecimal getPowerProductionAllowance() {
		return powerProductionAllowance;
	}

	public void setPowerProductionAllowance(BigDecimal powerProductionAllowance) {
		this.powerProductionAllowance = powerProductionAllowance;
	}

	public Boolean getFlgPowerMeasurement() {
		return flgPowerMeasurement;
	}

	public void setFlgPowerMeasurement(Boolean flgPowerMeasurement) {
		this.flgPowerMeasurement = flgPowerMeasurement;
	}

	public Boolean getFlgElevator() {
		return flgElevator;
	}

	public void setFlgElevator(Boolean flgElevator) {
		this.flgElevator = flgElevator;
	}

	public Boolean getFlgBreaker() {
		return flgBreaker;
	}

	public void setFlgBreaker(Boolean flgBreaker) {
		this.flgBreaker = flgBreaker;
	}

	public String getCodSupplyType() {
		return codSupplyType;
	}

	public void setCodSupplyType(String codSupplyType) {
		this.codSupplyType = codSupplyType;
	}

	public Boolean getFlgFlatRateBilling() {
		return flgFlatRateBilling;
	}

	public void setFlgFlatRateBilling(Boolean flgFlatRateBilling) {
		this.flgFlatRateBilling = flgFlatRateBilling;
	}

	public Boolean getFlgSeasonalSupply() {
		return flgSeasonalSupply;
	}

	public void setFlgSeasonalSupply(Boolean flgSeasonalSupply) {
		this.flgSeasonalSupply = flgSeasonalSupply;
	}

	public LocalDateTime getDtSeasonalSupplyStart() {
		return dtSeasonalSupplyStart;
	}

	public void setDtSeasonalSupplyStart(LocalDateTime dtSeasonalSupplyStart) {
		this.dtSeasonalSupplyStart = dtSeasonalSupplyStart;
	}

	public LocalDateTime getDtSeasonalSupplyEnd() {
		return dtSeasonalSupplyEnd;
	}

	public void setDtSeasonalSupplyEnd(LocalDateTime dtSeasonalSupplyEnd) {
		this.dtSeasonalSupplyEnd = dtSeasonalSupplyEnd;
	}

	public BigInteger getNumberOfTimeSlot() {
		return numberOfTimeSlot;
	}

	public void setNumberOfTimeSlot(BigInteger numberOfTimeSlot) {
		this.numberOfTimeSlot = numberOfTimeSlot;
	}

	public String getEidTariff() {
		return eidTariff;
	}

	public void setEidTariff(String eidTariff) {
		this.eidTariff = eidTariff;
	}

	public String getCodConnectionType() {
		return codConnectionType;
	}

	public void setCodConnectionType(String codConnectionType) {
		this.codConnectionType = codConnectionType;
	}

	public String getCodEnergyUse() {
		return codEnergyUse;
	}

	public void setCodEnergyUse(String codEnergyUse) {
		this.codEnergyUse = codEnergyUse;
	}

	public String getCodPayloadCurvesType() {
		return codPayloadCurvesType;
	}

	public void setCodPayloadCurvesType(String codPayloadCurvesType) {
		this.codPayloadCurvesType = codPayloadCurvesType;
	}

	public String getCodCommoditiesSector() {
		return codCommoditiesSector;
	}

	public void setCodCommoditiesSector(String codCommoditiesSector) {
		this.codCommoditiesSector = codCommoditiesSector;
	}

	public BigInteger getReductionFactor() {
		return reductionFactor;
	}

	public void setReductionFactor(BigInteger reductionFactor) {
		this.reductionFactor = reductionFactor;
	}

	public String getCodSupplyState() {
		return codSupplyState;
	}

	public void setCodSupplyState(String codSupplyState) {
		this.codSupplyState = codSupplyState;
	}

	public String getCodPaymentStatus() {
		return codPaymentStatus;
	}

	public void setCodPaymentStatus(String codPaymentStatus) {
		this.codPaymentStatus = codPaymentStatus;
	}

	public Boolean getFlgComplianceStatus() {
		return flgComplianceStatus;
	}

	public void setFlgComplianceStatus(Boolean flgComplianceStatus) {
		this.flgComplianceStatus = flgComplianceStatus;
	}

	public Boolean getFlgCheckingStatus() {
		return flgCheckingStatus;
	}

	public void setFlgCheckingStatus(Boolean flgCheckingStatus) {
		this.flgCheckingStatus = flgCheckingStatus;
	}

	public String getCodReadingFrequency() {
		return codReadingFrequency;
	}

	public void setCodReadingFrequency(String codReadingFrequency) {
		this.codReadingFrequency = codReadingFrequency;
	}

	public String getCodConnectionContractType() {
		return codConnectionContractType;
	}

	public void setCodConnectionContractType(String codConnectionContractType) {
		this.codConnectionContractType = codConnectionContractType;
	}

	public Boolean getFlgOnlyInput() {
		return flgOnlyInput;
	}

	public void setFlgOnlyInput(Boolean flgOnlyInput) {
		this.flgOnlyInput = flgOnlyInput;
	}

	public String getCodMeasurementPointType() {
		return codMeasurementPointType;
	}

	public void setCodMeasurementPointType(String codMeasurementPointType) {
		this.codMeasurementPointType = codMeasurementPointType;
	}

	public String getEidCensimpGroup() {
		return eidCensimpGroup;
	}

	public void setEidCensimpGroup(String eidCensimpGroup) {
		this.eidCensimpGroup = eidCensimpGroup;
	}

	public String getEidCensimpMeter() {
		return eidCensimpMeter;
	}

	public void setEidCensimpMeter(String eidCensimpMeter) {
		this.eidCensimpMeter = eidCensimpMeter;
	}

	public String getCodInstallationServiceType() {
		return codInstallationServiceType;
	}

	public void setCodInstallationServiceType(String codInstallationServiceType) {
		this.codInstallationServiceType = codInstallationServiceType;
	}

	public String getEidMeasurementPoint() {
		return eidMeasurementPoint;
	}

	public void setEidMeasurementPoint(String eidMeasurementPoint) {
		this.eidMeasurementPoint = eidMeasurementPoint;
	}

	public String getEidCensimpProductionPlant() {
		return eidCensimpProductionPlant;
	}

	public void setEidCensimpProductionPlant(String eidCensimpProductionPlant) {
		this.eidCensimpProductionPlant = eidCensimpProductionPlant;
	}

	public String getEidProductionSection() {
		return eidProductionSection;
	}

	public void setEidProductionSection(String eidProductionSection) {
		this.eidProductionSection = eidProductionSection;
	}

	public String getEidProductionUnit() {
		return eidProductionUnit;
	}

	public void setEidProductionUnit(String eidProductionUnit) {
		this.eidProductionUnit = eidProductionUnit;
	}

	public Boolean getFlgTemporaryExercise() {
		return flgTemporaryExercise;
	}

	public void setFlgTemporaryExercise(Boolean flgTemporaryExercise) {
		this.flgTemporaryExercise = flgTemporaryExercise;
	}

	public String getCodDetachmentType() {
		return codDetachmentType;
	}

	public void setCodDetachmentType(String codDetachmentType) {
		this.codDetachmentType = codDetachmentType;
	}

	public String getCodDetachmentResult() {
		return codDetachmentResult;
	}

	public void setCodDetachmentResult(String codDetachmentResult) {
		this.codDetachmentResult = codDetachmentResult;
	}

	public String getMeasureMngmntTechnical() {
		return measureMngmntTechnical;
	}

	public void setMeasureMngmntTechnical(String measureMngmntTechnical) {
		this.measureMngmntTechnical = measureMngmntTechnical;
	}

	public String getMeasureMngmntCommercial() {
		return measureMngmntCommercial;
	}

	public void setMeasureMngmntCommercial(String measureMngmntCommercial) {
		this.measureMngmntCommercial = measureMngmntCommercial;
	}

	public Boolean getFlgMeasurementService() {
		return flgMeasurementService;
	}

	public void setFlgMeasurementService(Boolean flgMeasurementService) {
		this.flgMeasurementService = flgMeasurementService;
	}

	public LocalDateTime getDtSupplyStart() {
		return dtSupplyStart;
	}

	public void setDtSupplyStart(LocalDateTime dtSupplyStart) {
		this.dtSupplyStart = dtSupplyStart;
	}

	public String getCodConnectionPointType() {
		return codConnectionPointType;
	}

	public void setCodConnectionPointType(String codConnectionPointType) {
		this.codConnectionPointType = codConnectionPointType;
	}

	public String getCodPowerMeasurementType() {
		return codPowerMeasurementType;
	}

	public void setCodPowerMeasurementType(String codPowerMeasurementType) {
		this.codPowerMeasurementType = codPowerMeasurementType;
	}

	public String getSaStreet() {
		return saStreet;
	}

	public void setSaStreet(String saStreet) {
		this.saStreet = saStreet;
	}

	public String getSaAddressNumber() {
		return saAddressNumber;
	}

	public void setSaAddressNumber(String saAddressNumber) {
		this.saAddressNumber = saAddressNumber;
	}

	public String getSaStair() {
		return saStair;
	}

	public void setSaStair(String saStair) {
		this.saStair = saStair;
	}

	public String getSaFloor() {
		return saFloor;
	}

	public void setSaFloor(String saFloor) {
		this.saFloor = saFloor;
	}

	public String getSaFlatNumber() {
		return saFlatNumber;
	}

	public void setSaFlatNumber(String saFlatNumber) {
		this.saFlatNumber = saFlatNumber;
	}

	public String getSaLocality() {
		return saLocality;
	}

	public void setSaLocality(String saLocality) {
		this.saLocality = saLocality;
	}

	public String getSaPrefix() {
		return saPrefix;
	}

	public void setSaPrefix(String saPrefix) {
		this.saPrefix = saPrefix;
	}

	public String getSaMunicipality() {
		return saMunicipality;
	}

	public void setSaMunicipality(String saMunicipality) {
		this.saMunicipality = saMunicipality;
	}

	public String getSaDistrict() {
		return saDistrict;
	}

	public void setSaDistrict(String saDistrict) {
		this.saDistrict = saDistrict;
	}

	public String getSaNation() {
		return saNation;
	}

	public void setSaNation(String saNation) {
		this.saNation = saNation;
	}

	public String getSaZipCode() {
		return saZipCode;
	}

	public void setSaZipCode(String saZipCode) {
		this.saZipCode = saZipCode;
	}

	public String getSaCodIstat() {
		return saCodIstat;
	}

	public void setSaCodIstat(String saCodIstat) {
		this.saCodIstat = saCodIstat;
	}

	public String getSaCareOf() {
		return saCareOf;
	}

	public void setSaCareOf(String saCareOf) {
		this.saCareOf = saCareOf;
	}

	public String getEidSocket() {
		return eidSocket;
	}

	public void setEidSocket(String eidSocket) {
		this.eidSocket = eidSocket;
	}

	public String getCodSocketPositionType() {
		return codSocketPositionType;
	}

	public void setCodSocketPositionType(String codSocketPositionType) {
		this.codSocketPositionType = codSocketPositionType;
	}

	public String getCpCodElectricPhases() {
		return cpCodElectricPhases;
	}

	public void setCpCodElectricPhases(String cpCodElectricPhases) {
		this.cpCodElectricPhases = cpCodElectricPhases;
	}

	public BigDecimal getCableSection() {
		return cableSection;
	}

	public void setCableSection(BigDecimal cableSection) {
		this.cableSection = cableSection;
	}

	public String getAccessInstruction() {
		return accessInstruction;
	}

	public void setAccessInstruction(String accessInstruction) {
		this.accessInstruction = accessInstruction;
	}

	public BigInteger getSubstationDistance() {
		return substationDistance;
	}

	public void setSubstationDistance(BigInteger substationDistance) {
		this.substationDistance = substationDistance;
	}

	public String getEidSubstation() {
		return eidSubstation;
	}

	public void setEidSubstation(String eidSubstation) {
		this.eidSubstation = eidSubstation;
	}

	public String getSubstationName() {
		return substationName;
	}

	public void setSubstationName(String substationName) {
		this.substationName = substationName;
	}

	public String getCodSubstationType() {
		return codSubstationType;
	}

	public void setCodSubstationType(String codSubstationType) {
		this.codSubstationType = codSubstationType;
	}

	public String getCodOpticFiberStatusType() {
		return codOpticFiberStatusType;
	}

	public void setCodOpticFiberStatusType(String codOpticFiberStatusType) {
		this.codOpticFiberStatusType = codOpticFiberStatusType;
	}

	public String getGpsCoordinateX() {
		return gpsCoordinateX;
	}

	public void setGpsCoordinateX(String gpsCoordinateX) {
		this.gpsCoordinateX = gpsCoordinateX;
	}

	public String getGpsCoordinateY() {
		return gpsCoordinateY;
	}

	public void setGpsCoordinateY(String gpsCoordinateY) {
		this.gpsCoordinateY = gpsCoordinateY;
	}

	public String getEidNetworkPoint() {
		return eidNetworkPoint;
	}

	public void setEidNetworkPoint(String eidNetworkPoint) {
		this.eidNetworkPoint = eidNetworkPoint;
	}

	public String getMeasurementPointDescription() {
		return measurementPointDescription;
	}

	public void setMeasurementPointDescription(String measurementPointDescription) {
		this.measurementPointDescription = measurementPointDescription;
	}

	public String getMeterSerialNumber() {
		return meterSerialNumber;
	}

	public void setMeterSerialNumber(String meterSerialNumber) {
		this.meterSerialNumber = meterSerialNumber;
	}

	public String getMeterCode() {
		return meterCode;
	}

	public void setMeterCode(String meterCode) {
		this.meterCode = meterCode;
	}

	public String getGageLimiter() {
		return gageLimiter;
	}

	public void setGageLimiter(String gageLimiter) {
		this.gageLimiter = gageLimiter;
	}

	public BigDecimal getCosineT() {
		return cosineT;
	}

	public void setCosineT(BigDecimal cosineT) {
		this.cosineT = cosineT;
	}

	public String getElectronicUnitRegisterNumber() {
		return electronicUnitRegisterNumber;
	}

	public void setElectronicUnitRegisterNumber(String electronicUnitRegisterNumber) {
		this.electronicUnitRegisterNumber = electronicUnitRegisterNumber;
	}

//	public Boolean getFlgElectronicUnit() {
//		return flgElectronicUnit;
//	}
//
//	public void setFlgElectronicUnit(Boolean flgElectronicUnit) {
//		this.flgElectronicUnit = flgElectronicUnit;
//	}

	public BigInteger getRegisterDisiDisconnectivity() {
		return registerDisiDisconnectivity;
	}

	public void setRegisterDisiDisconnectivity(BigInteger registerDisiDisconnectivity) {
		this.registerDisiDisconnectivity = registerDisiDisconnectivity;
	}

	public BigInteger getDigitsActiveEnergy() {
		return digitsActiveEnergy;
	}

	public void setDigitsActiveEnergy(BigInteger digitsActiveEnergy) {
		this.digitsActiveEnergy = digitsActiveEnergy;
	}

	public BigInteger getDigitsPower() {
		return digitsPower;
	}

	public void setDigitsPower(BigInteger digitsPower) {
		this.digitsPower = digitsPower;
	}

	public BigInteger getDigitsReactiveEnergy() {
		return digitsReactiveEnergy;
	}

	public void setDigitsReactiveEnergy(BigInteger digitsReactiveEnergy) {
		this.digitsReactiveEnergy = digitsReactiveEnergy;
	}

	public String getmNotes() {
		return mNotes;
	}

	public void setmNotes(String mNotes) {
		this.mNotes = mNotes;
	}

	public String getSoftwareVersion() {
		return softwareVersion;
	}

	public void setSoftwareVersion(String softwareVersion) {
		this.softwareVersion = softwareVersion;
	}

	public String getHardwareVersion() {
		return hardwareVersion;
	}

	public void setHardwareVersion(String hardwareVersion) {
		this.hardwareVersion = hardwareVersion;
	}

	public BigInteger getRegisterTsenAntitamper() {
		return registerTsenAntitamper;
	}

	public void setRegisterTsenAntitamper(BigInteger registerTsenAntitamper) {
		this.registerTsenAntitamper = registerTsenAntitamper;
	}

	public BigInteger getRegisterTsenmagAntitamper() {
		return registerTsenmagAntitamper;
	}

	public void setRegisterTsenmagAntitamper(BigInteger registerTsenmagAntitamper) {
		this.registerTsenmagAntitamper = registerTsenmagAntitamper;
	}

	public String getCodProtocolType() {
		return codProtocolType;
	}

	public void setCodProtocolType(String codProtocolType) {
		this.codProtocolType = codProtocolType;
	}

	public Boolean getFlgTechnicalBidirectional() {
		return flgTechnicalBidirectional;
	}

	public void setFlgTechnicalBidirectional(Boolean flgTechnicalBidirectional) {
		this.flgTechnicalBidirectional = flgTechnicalBidirectional;
	}

	public String getAnomaliesIrregList() {
		return anomaliesIrregList;
	}

	public void setAnomaliesIrregList(String anomaliesIrregList) {
		this.anomaliesIrregList = anomaliesIrregList;
	}

	public String getCodFindsEnvelope() {
		return codFindsEnvelope;
	}

	public void setCodFindsEnvelope(String codFindsEnvelope) {
		this.codFindsEnvelope = codFindsEnvelope;
	}

	public String getCodMeterType() {
		return codMeterType;
	}

	public void setCodMeterType(String codMeterType) {
		this.codMeterType = codMeterType;
	}

	public LocalDateTime getDtInstallation() {
		return dtInstallation;
	}

	public void setDtInstallation(LocalDateTime dtInstallation) {
		this.dtInstallation = dtInstallation;
	}

	public BigDecimal getVoltage() {
		return voltage;
	}

	public void setVoltage(BigDecimal voltage) {
		this.voltage = voltage;
	}

//	public Boolean getFlgPositiveMeasure() {
//		return flgPositiveMeasure;
//	}
//
//	public void setFlgPositiveMeasure(Boolean flgPositiveMeasure) {
//		this.flgPositiveMeasure = flgPositiveMeasure;
//	}

	public String getBuilderSerialNumber() {
		return builderSerialNumber;
	}

	public void setBuilderSerialNumber(String builderSerialNumber) {
		this.builderSerialNumber = builderSerialNumber;
	}

	public String getComModuleProtocol() {
		return comModuleProtocol;
	}

	public void setComModuleProtocol(String comModuleProtocol) {
		this.comModuleProtocol = comModuleProtocol;
	}

	public String getComModuleFrame() {
		return comModuleFrame;
	}

	public void setComModuleFrame(String comModuleFrame) {
		this.comModuleFrame = comModuleFrame;
	}

	public String getCsPhoneNumber() {
		return csPhoneNumber;
	}

	public void setCsPhoneNumber(String csPhoneNumber) {
		this.csPhoneNumber = csPhoneNumber;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

//	public BigInteger getProfile() {
//		return profile;
//	}
//
//	public void setProfile(BigInteger profile) {
//		this.profile = profile;
//	}

	public String getCodMeterCommManagerType() {
		return codMeterCommManagerType;
	}

	public void setCodMeterCommManagerType(String codMeterCommManagerType) {
		this.codMeterCommManagerType = codMeterCommManagerType;
	}

	public String getPlateSerialNumber() {
		return plateSerialNumber;
	}

	public void setPlateSerialNumber(String plateSerialNumber) {
		this.plateSerialNumber = plateSerialNumber;
	}

	public Boolean getFlgIgme() {
		return flgIgme;
	}

	public void setFlgIgme(Boolean flgIgme) {
		this.flgIgme = flgIgme;
	}

	public String getRegistersMeasureUnits() {
		return registersMeasureUnits;
	}

	public void setRegistersMeasureUnits(String registersMeasureUnits) {
		this.registersMeasureUnits = registersMeasureUnits;
	}

	public BigDecimal getInternalConstantCdc() {
		return internalConstantCdc;
	}

	public void setInternalConstantCdc(BigDecimal internalConstantCdc) {
		this.internalConstantCdc = internalConstantCdc;
	}

	public BigDecimal getInternalConstantRegisters() {
		return internalConstantRegisters;
	}

	public void setInternalConstantRegisters(BigDecimal internalConstantRegisters) {
		this.internalConstantRegisters = internalConstantRegisters;
	}

	public Boolean getFlgUseBuilderSerialNumber() {
		return flgUseBuilderSerialNumber;
	}

	public void setFlgUseBuilderSerialNumber(Boolean flgUseBuilderSerialNumber) {
		this.flgUseBuilderSerialNumber = flgUseBuilderSerialNumber;
	}

	public String getMultidropString() {
		return multidropString;
	}

	public void setMultidropString(String multidropString) {
		this.multidropString = multidropString;
	}

	public String getCodMeterOwner() {
		return codMeterOwner;
	}

	public void setCodMeterOwner(String codMeterOwner) {
		this.codMeterOwner = codMeterOwner;
	}


	public BigDecimal getMeterClass() {
		return meterClass;
	}

	public void setMeterClass(BigDecimal meterClass) {
		this.meterClass = meterClass;
	}

	public String getCodModuleBuilder() {
		return codModuleBuilder;
	}

	public void setCodModuleBuilder(String codModuleBuilder) {
		this.codModuleBuilder = codModuleBuilder;
	}

	public String getCodModuleModel() {
		return codModuleModel;
	}

	public void setCodModuleModel(String codModuleModel) {
		this.codModuleModel = codModuleModel;
	}

	public String getCodModuleCommunicationType() {
		return codModuleCommunicationType;
	}

	public void setCodModuleCommunicationType(String codModuleCommunicationType) {
		this.codModuleCommunicationType = codModuleCommunicationType;
	}

	public String getTechnicalContactName() {
		return technicalContactName;
	}

	public void setTechnicalContactName(String technicalContactName) {
		this.technicalContactName = technicalContactName;
	}

	public String getTechnicalContactPhone() {
		return technicalContactPhone;
	}

	public void setTechnicalContactPhone(String technicalContactPhone) {
		this.technicalContactPhone = technicalContactPhone;
	}

	public Boolean getFlgPasswordTeleReading() {
		return flgPasswordTeleReading;
	}

	public void setFlgPasswordTeleReading(Boolean flgPasswordTeleReading) {
		this.flgPasswordTeleReading = flgPasswordTeleReading;
	}

	public String getEidBuilder() {
		return eidBuilder;
	}

	public void setEidBuilder(String eidBuilder) {
		this.eidBuilder = eidBuilder;
	}

	public String getEidModel() {
		return eidModel;
	}

	public void setEidModel(String eidModel) {
		this.eidModel = eidModel;
	}

	public String getEidFirmwareVersion() {
		return eidFirmwareVersion;
	}

	public void setEidFirmwareVersion(String eidFirmwareVersion) {
		this.eidFirmwareVersion = eidFirmwareVersion;
	}

	public Boolean getFlgCommitting() {
		return flgCommitting;
	}

	public void setFlgCommitting(Boolean flgCommitting) {
		this.flgCommitting = flgCommitting;
	}

	public BigInteger getConstructionYear() {
		return constructionYear;
	}

	public void setConstructionYear(BigInteger constructionYear) {
		this.constructionYear = constructionYear;
	}

	public Boolean getFlgAntitamperActivated() {
		return flgAntitamperActivated;
	}

	public void setFlgAntitamperActivated(Boolean flgAntitamperActivated) {
		this.flgAntitamperActivated = flgAntitamperActivated;
	}

	public Boolean getFlgMagAntitamperActivated() {
		return flgMagAntitamperActivated;
	}

	public void setFlgMagAntitamperActivated(Boolean flgMagAntitamperActivated) {
		this.flgMagAntitamperActivated = flgMagAntitamperActivated;
	}

	public BigDecimal getPowerSumConstant() {
		return powerSumConstant;
	}

	public void setPowerSumConstant(BigDecimal powerSumConstant) {
		this.powerSumConstant = powerSumConstant;
	}

	public BigDecimal getEnergyConstant() {
		return energyConstant;
	}

	public void setEnergyConstant(BigDecimal energyConstant) {
		this.energyConstant = energyConstant;
	}

	public BigDecimal getPowerConstant() {
		return powerConstant;
	}

	public void setPowerConstant(BigDecimal powerConstant) {
		this.powerConstant = powerConstant;
	}

	public BigDecimal getFullScale() {
		return fullScale;
	}

	public void setFullScale(BigDecimal fullScale) {
		this.fullScale = fullScale;
	}

	public String getCaSerialNumberPhase1() {
		return caSerialNumberPhase1;
	}

	public void setCaSerialNumberPhase1(String caSerialNumberPhase1) {
		this.caSerialNumberPhase1 = caSerialNumberPhase1;
	}

	public String getCaSerialNumberPhase2() {
		return caSerialNumberPhase2;
	}

	public void setCaSerialNumberPhase2(String caSerialNumberPhase2) {
		this.caSerialNumberPhase2 = caSerialNumberPhase2;
	}

	public String getCaSerialNumberPhase3() {
		return caSerialNumberPhase3;
	}

	public void setCaSerialNumberPhase3(String caSerialNumberPhase3) {
		this.caSerialNumberPhase3 = caSerialNumberPhase3;
	}

	public String getCaSerialNumberPhase4() {
		return caSerialNumberPhase4;
	}

	public void setCaSerialNumberPhase4(String caSerialNumberPhase4) {
		this.caSerialNumberPhase4 = caSerialNumberPhase4;
	}

	public BigDecimal getKaAmpereRatio() {
		return kaAmpereRatio;
	}

	public void setKaAmpereRatio(BigDecimal kaAmpereRatio) {
		this.kaAmpereRatio = kaAmpereRatio;
	}

	public BigDecimal getCaPrimaryRatio() {
		return caPrimaryRatio;
	}

	public void setCaPrimaryRatio(BigDecimal caPrimaryRatio) {
		this.caPrimaryRatio = caPrimaryRatio;
	}

	public BigDecimal getCaSecondaryRatio() {
		return caSecondaryRatio;
	}

	public void setCaSecondaryRatio(BigDecimal caSecondaryRatio) {
		this.caSecondaryRatio = caSecondaryRatio;
	}

	public String getVaSerialNumberPhase1() {
		return vaSerialNumberPhase1;
	}

	public void setVaSerialNumberPhase1(String vaSerialNumberPhase1) {
		this.vaSerialNumberPhase1 = vaSerialNumberPhase1;
	}

	public String getVaSerialNumberPhase2() {
		return vaSerialNumberPhase2;
	}

	public void setVaSerialNumberPhase2(String vaSerialNumberPhase2) {
		this.vaSerialNumberPhase2 = vaSerialNumberPhase2;
	}

	public String getVaSerialNumberPhase3() {
		return vaSerialNumberPhase3;
	}

	public void setVaSerialNumberPhase3(String vaSerialNumberPhase3) {
		this.vaSerialNumberPhase3 = vaSerialNumberPhase3;
	}

	public BigDecimal getKvVoltageRatio() {
		return kvVoltageRatio;
	}

	public void setKvVoltageRatio(BigDecimal kvVoltageRatio) {
		this.kvVoltageRatio = kvVoltageRatio;
	}

	public BigInteger getVaPrimaryRatio() {
		return vaPrimaryRatio;
	}

	public void setVaPrimaryRatio(BigInteger vaPrimaryRatio) {
		this.vaPrimaryRatio = vaPrimaryRatio;
	}

	public BigInteger getVaSecondaryRatio() {
		return vaSecondaryRatio;
	}

	public void setVaSecondaryRatio(BigInteger vaSecondaryRatio) {
		this.vaSecondaryRatio = vaSecondaryRatio;
	}

	public BigInteger getKapi() {
		return kapi;
	}

	public void setKapi(BigInteger kapi) {
		this.kapi = kapi;
	}

	public BigInteger getKasi() {
		return kasi;
	}

	public void setKasi(BigInteger kasi) {
		this.kasi = kasi;
	}

	public BigDecimal getConstantKaXKv() {
		return constantKaXKv;
	}

	public void setConstantKaXKv(BigDecimal constantKaXKv) {
		this.constantKaXKv = constantKaXKv;
	}

	public String getCaBrandPhase1() {
		return caBrandPhase1;
	}

	public void setCaBrandPhase1(String caBrandPhase1) {
		this.caBrandPhase1 = caBrandPhase1;
	}

	public String getCaBrandPhase2() {
		return caBrandPhase2;
	}

	public void setCaBrandPhase2(String caBrandPhase2) {
		this.caBrandPhase2 = caBrandPhase2;
	}

	public String getCaBrandPhase3() {
		return caBrandPhase3;
	}

	public void setCaBrandPhase3(String caBrandPhase3) {
		this.caBrandPhase3 = caBrandPhase3;
	}

	public String getCaBrandPhase4() {
		return caBrandPhase4;
	}

	public void setCaBrandPhase4(String caBrandPhase4) {
		this.caBrandPhase4 = caBrandPhase4;
	}

	public String getCaModelPhase1() {
		return caModelPhase1;
	}

	public void setCaModelPhase1(String caModelPhase1) {
		this.caModelPhase1 = caModelPhase1;
	}

	public String getCaModelPhase2() {
		return caModelPhase2;
	}

	public void setCaModelPhase2(String caModelPhase2) {
		this.caModelPhase2 = caModelPhase2;
	}

	public String getCaModelPhase3() {
		return caModelPhase3;
	}

	public void setCaModelPhase3(String caModelPhase3) {
		this.caModelPhase3 = caModelPhase3;
	}

	public String getCaModelPhase4() {
		return caModelPhase4;
	}

	public void setCaModelPhase4(String caModelPhase4) {
		this.caModelPhase4 = caModelPhase4;
	}

	public String getCaClassPhase1() {
		return caClassPhase1;
	}

	public void setCaClassPhase1(String caClassPhase1) {
		this.caClassPhase1 = caClassPhase1;
	}

	public String getCaClassPhase2() {
		return caClassPhase2;
	}

	public void setCaClassPhase2(String caClassPhase2) {
		this.caClassPhase2 = caClassPhase2;
	}

	public String getCaClassPhase3() {
		return caClassPhase3;
	}

	public void setCaClassPhase3(String caClassPhase3) {
		this.caClassPhase3 = caClassPhase3;
	}

	public String getCaClassPhase4() {
		return caClassPhase4;
	}

	public void setCaClassPhase4(String caClassPhase4) {
		this.caClassPhase4 = caClassPhase4;
	}

	public String getVaBrandPhase1() {
		return vaBrandPhase1;
	}

	public void setVaBrandPhase1(String vaBrandPhase1) {
		this.vaBrandPhase1 = vaBrandPhase1;
	}

	public String getVaBrandPhase2() {
		return vaBrandPhase2;
	}

	public void setVaBrandPhase2(String vaBrandPhase2) {
		this.vaBrandPhase2 = vaBrandPhase2;
	}

	public String getVaBrandPhase3() {
		return vaBrandPhase3;
	}

	public void setVaBrandPhase3(String vaBrandPhase3) {
		this.vaBrandPhase3 = vaBrandPhase3;
	}

	public String getVaModelPhase1() {
		return vaModelPhase1;
	}

	public void setVaModelPhase1(String vaModelPhase1) {
		this.vaModelPhase1 = vaModelPhase1;
	}

	public String getVaModelPhase2() {
		return vaModelPhase2;
	}

	public void setVaModelPhase2(String vaModelPhase2) {
		this.vaModelPhase2 = vaModelPhase2;
	}

	public String getVaModelPhase3() {
		return vaModelPhase3;
	}

	public void setVaModelPhase3(String vaModelPhase3) {
		this.vaModelPhase3 = vaModelPhase3;
	}

	public String getVaClassPhase1() {
		return vaClassPhase1;
	}

	public void setVaClassPhase1(String vaClassPhase1) {
		this.vaClassPhase1 = vaClassPhase1;
	}

	public String getVaClassPhase2() {
		return vaClassPhase2;
	}

	public void setVaClassPhase2(String vaClassPhase2) {
		this.vaClassPhase2 = vaClassPhase2;
	}

	public String getVaClassPhase3() {
		return vaClassPhase3;
	}

	public void setVaClassPhase3(String vaClassPhase3) {
		this.vaClassPhase3 = vaClassPhase3;
	}

	public BigInteger getOverchargeabilityPercentage() {
		return overchargeabilityPercentage;
	}

	public void setOverchargeabilityPercentage(BigInteger overchargeabilityPercentage) {
		this.overchargeabilityPercentage = overchargeabilityPercentage;
	}

	public String getCodDeviceOwnerType() {
		return codDeviceOwnerType;
	}

	public void setCodDeviceOwnerType(String codDeviceOwnerType) {
		this.codDeviceOwnerType = codDeviceOwnerType;
	}

	public String getCodModuleProtocolType() {
		return codModuleProtocolType;
	}

	public void setCodModuleProtocolType(String codModuleProtocolType) {
		this.codModuleProtocolType = codModuleProtocolType;
	}

	public Boolean getFlgExternalModule() {
		return flgExternalModule;
	}

	public void setFlgExternalModule(Boolean flgExternalModule) {
		this.flgExternalModule = flgExternalModule;
	}

	public Boolean getFlgPstnProtection() {
		return flgPstnProtection;
	}

	public void setFlgPstnProtection(Boolean flgPstnProtection) {
		this.flgPstnProtection = flgPstnProtection;
	}

	public String getCcid() {
		return ccid;
	}

	public void setCcid(String ccid) {
		this.ccid = ccid;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getPuk() {
		return puk;
	}

	public void setPuk(String puk) {
		this.puk = puk;
	}

	public String getAssigneeName() {
		return assigneeName;
	}

	public void setAssigneeName(String assigneeName) {
		this.assigneeName = assigneeName;
	}

	public LocalDateTime getDtAssign() {
		return dtAssign;
	}

	public void setDtAssign(LocalDateTime dtAssign) {
		this.dtAssign = dtAssign;
	}

	public String getImsi() {
		return imsi;
	}

	public void setImsi(String imsi) {
		this.imsi = imsi;
	}

	public String getIpv4() {
		return ipv4;
	}

	public void setIpv4(String ipv4) {
		this.ipv4 = ipv4;
	}

	public String getIpv6() {
		return ipv6;
	}

	public void setIpv6(String ipv6) {
		this.ipv6 = ipv6;
	}

	public String getLoadingAuthor() {
		return loadingAuthor;
	}

	public void setLoadingAuthor(String loadingAuthor) {
		this.loadingAuthor = loadingAuthor;
	}

	public LocalDateTime getDtLoading() {
		return dtLoading;
	}

	public void setDtLoading(LocalDateTime dtLoading) {
		this.dtLoading = dtLoading;
	}

	public String getCodTelcoProvider() {
		return codTelcoProvider;
	}

	public void setCodTelcoProvider(String codTelcoProvider) {
		this.codTelcoProvider = codTelcoProvider;
	}

	public String getAutorCreation() {
		return autorCreation;
	}

	public void setAutorCreation(String autorCreation) {
		this.autorCreation = autorCreation;
	}

	public String getEneltelOfDelivery() {
		return eneltelOfDelivery;
	}

	public void setEneltelOfDelivery(String eneltelOfDelivery) {
		this.eneltelOfDelivery = eneltelOfDelivery;
	}

	public String getCodContractServiceType() {
		return codContractServiceType;
	}

	public void setCodContractServiceType(String codContractServiceType) {
		this.codContractServiceType = codContractServiceType;
	}

	public String getCodMeterCodeType() {
		return codMeterCodeType;
	}

	public void setCodMeterCodeType(String codMeterCodeType) {
		this.codMeterCodeType = codMeterCodeType;
	}

	public String getCodSocketGroup() {
		return codSocketGroup;
	}

	public void setCodSocketGroup(String codSocketGroup) {
		this.codSocketGroup = codSocketGroup;
	}

	public LocalDateTime getDtMeterRemoved() {
		return dtMeterRemoved;
	}

	public void setDtMeterRemoved(LocalDateTime dtMeterRemoved) {
		this.dtMeterRemoved = dtMeterRemoved;
	}

	public BigInteger getEidTrader() {
		return eidTrader;
	}

	public void setEidTrader(BigInteger eidTrader) {
		this.eidTrader = eidTrader;
	}

	public Boolean getFlgInstalled() {
		return flgInstalled;
	}

	public void setFlgInstalled(Boolean flgInstalled) {
		this.flgInstalled = flgInstalled;
	}

	public Boolean getFlgRealPoint() {
		return flgRealPoint;
	}

	public void setFlgRealPoint(Boolean flgRealPoint) {
		this.flgRealPoint = flgRealPoint;
	}

	public Boolean getFlgSupendedForChecking() {
		return flgSupendedForChecking;
	}

	public void setFlgSupendedForChecking(Boolean flgSupendedForChecking) {
		this.flgSupendedForChecking = flgSupendedForChecking;
	}

	public String getCodEnergyBalance() {
		return codEnergyBalance;
	}

	public void setCodEnergyBalance(String codEnergyBalance) {
		this.codEnergyBalance = codEnergyBalance;
	}

	public String getK2Key() {
		return k2Key;
	}

	public void setK2Key(String k2Key) {
		this.k2Key = k2Key;
	}

	public String getMaAddressNumber() {
		return maAddressNumber;
	}

	public void setMaAddressNumber(String maAddressNumber) {
		this.maAddressNumber = maAddressNumber;
	}

	public String getMaCareOf() {
		return maCareOf;
	}

	public void setMaCareOf(String maCareOf) {
		this.maCareOf = maCareOf;
	}

	public String getMaCodIstat() {
		return maCodIstat;
	}

	public void setMaCodIstat(String maCodIstat) {
		this.maCodIstat = maCodIstat;
	}

	public String getMaDistrict() {
		return maDistrict;
	}

	public void setMaDistrict(String maDistrict) {
		this.maDistrict = maDistrict;
	}

	public String getMaFlatNumber() {
		return maFlatNumber;
	}

	public void setMaFlatNumber(String maFlatNumber) {
		this.maFlatNumber = maFlatNumber;
	}

	public String getMaFloor() {
		return maFloor;
	}

	public void setMaFloor(String maFloor) {
		this.maFloor = maFloor;
	}

	public String getMaLocality() {
		return maLocality;
	}

	public void setMaLocality(String maLocality) {
		this.maLocality = maLocality;
	}

	public String getMaMunicipality() {
		return maMunicipality;
	}

	public void setMaMunicipality(String maMunicipality) {
		this.maMunicipality = maMunicipality;
	}

	public String getMaNation() {
		return maNation;
	}

	public void setMaNation(String maNation) {
		this.maNation = maNation;
	}

	public String getMaPrefix() {
		return maPrefix;
	}

	public void setMaPrefix(String maPrefix) {
		this.maPrefix = maPrefix;
	}

	public String getMaRegion() {
		return maRegion;
	}

	public void setMaRegion(String maRegion) {
		this.maRegion = maRegion;
	}

	public String getMaStair() {
		return maStair;
	}

	public void setMaStair(String maStair) {
		this.maStair = maStair;
	}

	public String getMaStreet() {
		return maStreet;
	}

	public void setMaStreet(String maStreet) {
		this.maStreet = maStreet;
	}

	public String getMaZipCode() {
		return maZipCode;
	}

	public void setMaZipCode(String maZipCode) {
		this.maZipCode = maZipCode;
	}

	public LocalDateTime getProdContrActivationDate() {
		return prodContrActivationDate;
	}

	public void setProdContrActivationDate(LocalDateTime prodContrActivationDate) {
		this.prodContrActivationDate = prodContrActivationDate;
	}

	public LocalDateTime getProdContrEndExerciseRegulationDate() {
		return prodContrEndExerciseRegulationDate;
	}

	public void setProdContrEndExerciseRegulationDate(LocalDateTime prodContrEndExerciseRegulationDate) {
		this.prodContrEndExerciseRegulationDate = prodContrEndExerciseRegulationDate;
	}

	public LocalDateTime getProdContrTerminationDate() {
		return prodContrTerminationDate;
	}

	public void setProdContrTerminationDate(LocalDateTime prodContrTerminationDate) {
		this.prodContrTerminationDate = prodContrTerminationDate;
	}

	public String getProdContrTerminationReason() {
		return prodContrTerminationReason;
	}

	public void setProdContrTerminationReason(String prodContrTerminationReason) {
		this.prodContrTerminationReason = prodContrTerminationReason;
	}

	public BigDecimal getProdContrTransientPower() {
		return prodContrTransientPower;
	}

	public void setProdContrTransientPower(BigDecimal prodContrTransientPower) {
		this.prodContrTransientPower = prodContrTransientPower;
	}

	public String getProdContrType() {
		return prodContrType;
	}

	public void setProdContrType(String prodContrType) {
		this.prodContrType = prodContrType;
	}

	public BigInteger getSupplyVoltage() {
		return supplyVoltage;
	}

	public void setSupplyVoltage(BigInteger supplyVoltage) {
		this.supplyVoltage = supplyVoltage;
	}

	public LocalDateTime getTitleExpirationDate() {
		return titleExpirationDate;
	}

	public void setTitleExpirationDate(LocalDateTime titleExpirationDate) {
		this.titleExpirationDate = titleExpirationDate;
	}

	public String getTraderCodMarketType() {
		return traderCodMarketType;
	}

	public void setTraderCodMarketType(String traderCodMarketType) {
		this.traderCodMarketType = traderCodMarketType;
	}

	public String getTraderCompanyName() {
		return traderCompanyName;
	}

	public void setTraderCompanyName(String traderCompanyName) {
		this.traderCompanyName = traderCompanyName;
	}

	public String getTraderMobilePhoneNumber() {
		return traderMobilePhoneNumber;
	}

	public void setTraderMobilePhoneNumber(String traderMobilePhoneNumber) {
		this.traderMobilePhoneNumber = traderMobilePhoneNumber;
	}

	public String getTraderPhoneNumber() {
		return traderPhoneNumber;
	}

	public void setTraderPhoneNumber(String traderPhoneNumber) {
		this.traderPhoneNumber = traderPhoneNumber;
	}

	public String getTraderVatNumber() {
		return traderVatNumber;
	}

	public void setTraderVatNumber(String traderVatNumber) {
		this.traderVatNumber = traderVatNumber;
	}

	public String getVaBrandPhase4() {
		return vaBrandPhase4;
	}

	public void setVaBrandPhase4(String vaBrandPhase4) {
		this.vaBrandPhase4 = vaBrandPhase4;
	}

	public String getVaClassPhase4() {
		return vaClassPhase4;
	}

	public void setVaClassPhase4(String vaClassPhase4) {
		this.vaClassPhase4 = vaClassPhase4;
	}

	public String getVaModelPhase4() {
		return vaModelPhase4;
	}

	public void setVaModelPhase4(String vaModelPhase4) {
		this.vaModelPhase4 = vaModelPhase4;
	}

	public String getVaSerialNumberPhase4() {
		return vaSerialNumberPhase4;
	}

	public void setVaSerialNumberPhase4(String vaSerialNumberPhase4) {
		this.vaSerialNumberPhase4 = vaSerialNumberPhase4;
	}

	public String getCodModuleFrameType() {
		return codModuleFrameType;
	}

	public void setCodModuleFrameType(String codModuleFrameType) {
		this.codModuleFrameType = codModuleFrameType;
	}

	public String getCodSpacerState() {
		return codSpacerState;
	}

	public void setCodSpacerState(String codSpacerState) {
		this.codSpacerState = codSpacerState;
	}

	public String getCodMeterDirectionType() {
		return codMeterDirectionType;
	}

	public void setCodMeterDirectionType(String codMeterDirectionType) {
		this.codMeterDirectionType = codMeterDirectionType;
	}

	public String getUnitCdc() {
		return unitCdc;
	}

	public void setUnitCdc(String unitCdc) {
		this.unitCdc = unitCdc;
	}

	public String getCodDirectionType() {
		return codDirectionType;
	}

	public void setCodDirectionType(String codDirectionType) {
		this.codDirectionType = codDirectionType;
	}

	public Boolean getFlgReacheable() {
		return flgReacheable;
	}

	public void setFlgReacheable(Boolean flgReacheable) {
		this.flgReacheable = flgReacheable;
	}

	public String getMeterSerialNumberReactive() {
		return meterSerialNumberReactive;
	}

	public void setMeterSerialNumberReactive(String meterSerialNumberReactive) {
		this.meterSerialNumberReactive = meterSerialNumberReactive;
	}

	public String getMeterCodeReactive() {
		return meterCodeReactive;
	}

	public void setMeterCodeReactive(String meterCodeReactive) {
		this.meterCodeReactive = meterCodeReactive;
	}

	public String getMeterSerialNumberPower() {
		return meterSerialNumberPower;
	}

	public void setMeterSerialNumberPower(String meterSerialNumberPower) {
		this.meterSerialNumberPower = meterSerialNumberPower;
	}

	public String getMeterCodePower() {
		return meterCodePower;
	}

	public void setMeterCodePower(String meterCodePower) {
		this.meterCodePower = meterCodePower;
	}

	public String getCodSocketState() {
		return codSocketState;
	}

	public void setCodSocketState(String codSocketState) {
		this.codSocketState = codSocketState;
	}

	public String getCodRemoteControlType() {
		return codRemoteControlType;
	}

	public void setCodRemoteControlType(String codRemoteControlType) {
		this.codRemoteControlType = codRemoteControlType;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public LocalDateTime getExtractiondate() {
		return extractiondate;
	}

	public void setExtractiondate(LocalDateTime extractiondate) {
		this.extractiondate = extractiondate;
	}

	public LocalDateTime getStartReferenceDate() {
		return startReferenceDate;
	}

	public void setStartReferenceDate(LocalDateTime startReferenceDate) {
		this.startReferenceDate = startReferenceDate;
	}

	public LocalDateTime getEndReferenceDate() {
		return endReferenceDate;
	}

	public void setEndReferenceDate(LocalDateTime endReferenceDate) {
		this.endReferenceDate = endReferenceDate;
	}

	public LocalDateTime getDtSynchronization() {
		return dtSynchronization;
	}

	public void setDtSynchronization(LocalDateTime dtSynchronization) {
		this.dtSynchronization = dtSynchronization;
	}

	public String getsNotes() {
		return sNotes;
	}

	public void setsNotes(String sNotes) {
		this.sNotes = sNotes;
	}

	public LocalDateTime getDate2gActivation() {
		return date2gActivation;
	}

	public void setDate2gActivation(LocalDateTime date2gActivation) {
		this.date2gActivation = date2gActivation;
	}

	public Boolean getFlgMeterGeneration2g() {
		return flgMeterGeneration2g;
	}

	public void setFlgMeterGeneration2g(Boolean flgMeterGeneration2g) {
		this.flgMeterGeneration2g = flgMeterGeneration2g;
	}

	public BigInteger getIdMdMeasurementPointHeartBeat() {
		return idMdMeasurementPointHeartBeat;
	}

	public void setIdMdMeasurementPointHeartBeat(BigInteger idMdMeasurementPointHeartBeat) {
		this.idMdMeasurementPointHeartBeat = idMdMeasurementPointHeartBeat;
	}

	public String getEidTransformer() {
		return eidTransformer;
	}

	public void setEidTransformer(String eidTransformer) {
		this.eidTransformer = eidTransformer;
	}

	public Boolean getFlgPointCodeDel8716() {
		return flgPointCodeDel8716;
	}

	public void setFlgPointCodeDel8716(Boolean flgPointCodeDel8716) {
		this.flgPointCodeDel8716 = flgPointCodeDel8716;
	}

	public String getCodButtonListType() {
		return codButtonListType;
	}

	public void setCodButtonListType(String codButtonListType) {
		this.codButtonListType = codButtonListType;
	}

	public LocalDateTime getDtButtonListReprogramming() {
		return dtButtonListReprogramming;
	}

	public void setDtButtonListReprogramming(LocalDateTime dtButtonListReprogramming) {
		this.dtButtonListReprogramming = dtButtonListReprogramming;
	}

	public String getCodMeterGeneration() {
		return codMeterGeneration;
	}

	public void setCodMeterGeneration(String codMeterGeneration) {
		this.codMeterGeneration = codMeterGeneration;
	}

	public Boolean getFlgFraudPredisposition() {
		return flgFraudPredisposition;
	}

	public void setFlgFraudPredisposition(Boolean flgFraudPredisposition) {
		this.flgFraudPredisposition = flgFraudPredisposition;
	}

}